export enum Role {
  Admin = "Admin",
  User = "User",
  Patient = "Patient",
}
