import { User } from "app/core/models/user";

export interface ListOfRequest extends Array<RequestComponent> {}
export interface RequestComponent {
  id: number;
  user: User;
  requestType: RequestType;
  description: string;
  note: string;
  status: number;
}
export interface RequestType {
  id: number;
  description: string;
  requestTypeName: string;
}