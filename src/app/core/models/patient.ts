export interface PatientResponse {
  code: number;
  data: Patient[] | any;
  message: string;
}
export interface PatientObject {
  data: Patient;
}
export interface Patient {
  userId: String;
  role: Role;
  username: String;
  password: String;
  email: String;
  phone: String;
  identityCard: null;
  firstname: String;
  surname: String;
  lastname: String;
  address: String;
  avatar: null;
  isActive: number;
  isOnline: number;
}
export interface Role {
  roleId: number;
  description: String;
  isActive: number;
}
