export interface MedicineResponse {
  code: string;
  message: string;
  data: Medicine;
}
export interface MedicinesResponse {
  code: string;
  message: string;
  data: Medicine[];
}
export interface MedicineObject {
  data: Medicine;
}
export interface Medicine {
  id: number;
  name: string;
  detail: string;
  description: string;
  thumbnail: string;
  isActive: number;
}
