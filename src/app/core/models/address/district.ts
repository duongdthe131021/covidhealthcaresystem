import { Province } from "./province";
export interface DistrictResponse {
  code: string;
  data: District;
  message: string;
}
export interface DistrictsResponse {
  code: string;
  data: District[];
  message: string;
}
export interface ListOfDistrict extends Array<District> {}

export interface District {
  districtId: number;
  name: string;
  province: Province;
  isActive: number;
}
