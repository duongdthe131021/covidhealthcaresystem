export interface ExerciseResponse {
  code: string;
  data: Exercise;
  message: string;
}
export interface ExercisesResponse {
  code: string;
  data: Exercise[];
  message: string;
}

export interface Exercise {
  exerciseId: number;
  id: number;
  name: string;
  description: string;
  thumbnail: string;
}
export interface ExerciseStepsResponse{
  code: number;
  data: ExerciseStep[];
}
export interface ExerciseStepResponse{
  code: number;
  data: ExerciseStep;
}
export interface ExerciseStep {
  detail: string;
  description: string;
  exercise: Exercise;
  exerciseDetailId: number;
  imageDescription: string;
  imageBase64?: string;
}

