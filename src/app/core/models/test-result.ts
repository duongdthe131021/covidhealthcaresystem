import { Village } from "./address/village";
import { User } from "./user";

export interface TestResultRespone {
  code: number;
  message: string;
  data: TestResult[];
}
export interface TestResultObj {
  data: TestResult;
}
export interface ListOfResult extends Array<TestResult> {}

export interface TestResult {
  resultId: number;
  resultCode: string;
  user: User;
  unit: {
    unitId: number;
    name: string;
    address: string;
    village: Village;
    isActive: 1;
  };
  numberTest: number;
  startDate: Date;
  collectDate: Date;
  receiveDate: Date;
  testDate: Date;
  sampleType: {
    typeId: number;
    name: string;
    isActive: number;
  };
  status: number;
  testResult: number;
  comment: string;
  isActive: number;
}
