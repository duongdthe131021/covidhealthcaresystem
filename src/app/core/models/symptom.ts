
export interface SymptomResponse{
    code: string;
    data: Symptom;
}
export interface SymptomsResponse{
  code: number;
  data: Symptom[];
}
export interface Symptom {
  symptomId: number;
  name: string;
  description: string;
  isActive: number;
}
