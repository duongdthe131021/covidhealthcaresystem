import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from "@angular/router";
import { TokenStorageService } from "./token-storage.service";

@Injectable({
  providedIn: "root",
})
export class SuperAdminGuardService {
  constructor(
    private router: Router,
    private tokenStorage: TokenStorageService
  ) {}
  superAdminActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const currentUser = this.tokenStorage.getUser();
    const role = this.tokenStorage.getRole();
    if (
      (currentUser && role === "Super admin")
    ) {
      return true;
    } else {
      return false;
    }
  }
}
