import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { Router } from "@angular/router";
import { UserResponse } from "app/core/models/user";

const TOKEN_KEY = "auth-token";
const USER_KEY = "auth-user";
const ROLE_KEY = "auth-role";


@Injectable({
  providedIn: "root",
})
export class TokenStorageService {
  isLogin$ = new BehaviorSubject<boolean>(false);
  constructor(private router: Router) {}

  signOut(): void {
    localStorage.clear();
  }

  public saveToken(token: string): void {
    localStorage.removeItem(TOKEN_KEY);
    localStorage.setItem(TOKEN_KEY, token);
  }

  public getToken(): string | null {
    return localStorage.getItem(TOKEN_KEY);
  }

  public saveUser(user: UserResponse): void {
    localStorage.removeItem(USER_KEY);
    localStorage.setItem(USER_KEY, JSON.stringify(user));
  }

  public getUser(): any {
    const user = localStorage.getItem(USER_KEY);
    if (user) {
      return JSON.parse(user);
    }
  }
  public saveRole(role: string): void {
    localStorage.removeItem(ROLE_KEY);
    localStorage.setItem(ROLE_KEY, role);
  }
  public getRole(): string | null {
    return localStorage.getItem(ROLE_KEY);
  }
}
