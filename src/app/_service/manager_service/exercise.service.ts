import {
  Exercise,
  ExerciseResponse,
  ExercisesResponse,
  ExerciseStepResponse,
  ExerciseStepsResponse,
} from "./../../core/models/exercise";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { TokenStorageService } from "../JWT_service/token-storage.service";
import { CONSTANT } from "app/core/constants";
@Injectable({
  providedIn: "root",
})
export class ExerciseService {
  getHeaders = () =>
    new HttpHeaders({
      Authorization: `Bearer ${this.TokenStorageService.getToken()}`,
    });

  constructor(
    private http: HttpClient,
    private TokenStorageService: TokenStorageService
  ) {}

  //Exercise service
  getExercise(): Observable<ExercisesResponse> {
    return this.http.get<ExercisesResponse>(
      `${CONSTANT.BASE_URL}/v1/api/exercise/get-all`,
      { headers: this.getHeaders() }
    );
  }
  getExerciseById(params): Observable<ExerciseResponse> {
    return this.http.get<ExerciseResponse>(
      `${CONSTANT.BASE_URL}/v1/api/exercise/get?exerciseId=${params}`,
      { headers: this.getHeaders() }
    );
  }
  getExerciseDetailById(params): Observable<ExerciseStepsResponse> {
    return this.http.get<ExerciseStepsResponse>(
      `${CONSTANT.BASE_URL}/v1/api/exercise_detail/get?exerciseId=${params}`,
      { headers: this.getHeaders() }
    );
  }
  exerciseUpdate(form: any): Observable<any> {
    return this.http.post<any>(
      `${CONSTANT.BASE_URL}/v1/api/exercise/update`,
      form,
      { headers: this.getHeaders() }
    );
  }
  exerciseCreate(form: any): Observable<any> {
    return this.http.post<any>(
      `${CONSTANT.BASE_URL}/v1/api/exercise/create`,
      form,
      { headers: this.getHeaders() }
    );
  }
  deleteExerciseById(params): Observable<any> {
    return this.http.put<any>(
      `${CONSTANT.BASE_URL}/v1/api/exercise/delete?exerciseId=${params}`,
      { headers: this.getHeaders() }
    );
  }

  //exercise step
  getExerciseStepDetailById(params): Observable<ExerciseResponse> {
    return this.http.get<ExerciseResponse>(
      `${CONSTANT.BASE_URL}/v1/api/exercise_detail/get?exerciseId=${params}`,
      { headers: this.getHeaders() }
    );
  }
  exerciseStepUpdate(form: any, params): Observable<any> {
    return this.http.post<any>(
      `${CONSTANT.BASE_URL}/v1/api/exercise_detail/update?exerciseDetailId=${params}`,
      form,
      { headers: this.getHeaders() }
    );
  }
  exerciseStepCreate(form: any): Observable<any> {
    return this.http.post<any>(
      `${CONSTANT.BASE_URL}/v1/api/exercise_detail/create`,
      form,
      { headers: this.getHeaders(), responseType: 'string' as 'json'  }
    );
  }
}
