import {
  DailyReports,
} from "./../../core/models/dailyReport";
import { Injectable } from "@angular/core";
import { TokenStorageService } from "../JWT_service/token-storage.service";
import { Observable } from "rxjs";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { SymptomResponse, SymptomsResponse } from "app/core/models/symptom";
import { CONSTANT } from "app/core/constants";

@Injectable({
  providedIn: "root",
})
export class ReportService {
  getHeaders = () =>
    new HttpHeaders({
      Authorization: `Bearer ${this.TokenStorageService.getToken()}`,
    });

  constructor(
    private http: HttpClient,
    private TokenStorageService: TokenStorageService
  ) {}

  // getDailyReport(): Observable<DailyReportResponse> {
  //   const dailyReportdata = this.http.get<DailyReportResponse>(
  //     `${CONSTANT.BASE_URL}/v1/api/daily-report/get-all`,
  //     { headers: this.getHeaders() }
  //   );
  //   return dailyReportdata;
  // }
  getReportByVillageId(params): Observable<DailyReports> {
    const dailyReportdata = this.http.post<DailyReports>(
      `${CONSTANT.BASE_URL}/v1/api/report/get/village?villageId=${params}`,
      {},
      { headers: this.getHeaders() }
    );
    return dailyReportdata;
  }

  // getDailyReportById(params): Observable<DailyReportObject> {
  //   const dailyReportdata = this.http.get<DailyReportObject>(
  //     `${CONSTANT.BASE_URL}/v1/api/daily-report/get?dailyReportId=${params}`,
  //     { headers: this.getHeaders() }
  //   );
  //   return dailyReportdata;
  // }
  getReportByUserId(params): Observable<DailyReports> {
    return this.http.post<DailyReports>(
      `${CONSTANT.BASE_URL}/v1/api/report/get/user?userId=${params}`,
      {},
      { headers: this.getHeaders() }
    );
  }

  
}
