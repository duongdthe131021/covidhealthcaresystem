import { HttpHeaders, HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { CONSTANT } from "app/core/constants";
import { SymptomsResponse, SymptomResponse } from "app/core/models/symptom";
import { Observable } from "rxjs";
import { TokenStorageService } from "../JWT_service/token-storage.service";
@Injectable({
  providedIn: "root",
})
export class SymptomService {
  getHeaders = () =>
    new HttpHeaders({
      Authorization: `Bearer ${this.TokenStorageService.getToken()}`,
    });
  constructor(
    private http: HttpClient,
    private TokenStorageService: TokenStorageService
  ) {}
  
  //Symptom service
  getSymptom(): Observable<SymptomsResponse> {
    return this.http.get<SymptomsResponse>(
      `${CONSTANT.BASE_URL}/v1/api/symptom/get-all`,
      { headers: this.getHeaders() }
    );
  }
  getSymptomById(params): Observable<SymptomResponse> {
    return this.http.get<SymptomResponse>(
      `${CONSTANT.BASE_URL}/v1/api/symptom/get?symptomId=${params}`,
      { headers: this.getHeaders() }
    );
  }
  symptomUpdate(form: any): Observable<SymptomResponse> {
    return this.http.post<SymptomResponse>(
      `${CONSTANT.BASE_URL}/v1/api/symptom/update`,
      form,
      { headers: this.getHeaders() }
    );
  }
  symptomCreate(form: any): Observable<SymptomResponse> {
    return this.http.post<SymptomResponse>(
      `${CONSTANT.BASE_URL}/v1/api/symptom/create`,
      form,
      { headers: this.getHeaders() }
    );
  }
}
