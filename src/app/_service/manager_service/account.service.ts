import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { CONSTANT } from "app/core/constants";

import {
  ListOfUser,
  ListOfUserResponse,
  User,
  UserObj,
  UserResponse,
} from "app/core/models/user";
import { Observable } from "rxjs";
import { TokenStorageService } from "../JWT_service/token-storage.service";

@Injectable({
  providedIn: "root",
})
export class AccountService {
  getHeaders = () =>
    new HttpHeaders({
      Authorization: `Bearer ${this.TokenStorageService.getToken()}`,
    });

  constructor(
    private http: HttpClient,
    private TokenStorageService: TokenStorageService
  ) {}

  getMedicalStaff2(params): Observable<ListOfUser> {
    return this.http.get<ListOfUser>(
      `${CONSTANT.BASE_URL}/v1/api/medical-staff/get-by-district?districtId=${params}`,
      { headers: this.getHeaders() }
    );
  }
  getMedicalStaff(params): Observable<ListOfUserResponse> {
    return this.http.get<ListOfUserResponse>(
      `${CONSTANT.BASE_URL}/v1/api/user/get-medical-staff?districtId=${params}`,
      { headers: this.getHeaders() }
    );
  }

  getPatientF0(): Observable<UserResponse> {
    return this.http.get<UserResponse>(
      `${CONSTANT.BASE_URL}/v1/api/user/get-patient`,
      { headers: this.getHeaders() }
    );
  }
  getPatientF1(): Observable<UserResponse> {
    return this.http.get<UserResponse>(
      `${CONSTANT.BASE_URL}/v1/api/user/get-patient`,
      { headers: this.getHeaders() }
    );
  }
  getUserById(params): Observable<UserResponse> {
    return this.http.post<UserResponse>(
      `${CONSTANT.BASE_URL}/v1/api/user/get-by-id/${params}`,
      {},
      { headers: this.getHeaders() }
    );
  }
  userUpdate(form: any): Observable<UserObj> {
    return this.http.post<UserObj>(
      `${CONSTANT.BASE_URL}/v1/api/user/update`,
      form,
      { headers: this.getHeaders() }
    );
  }

  accountCreate(form: any): Observable<UserObj> {
    return this.http.post<UserObj>(
      `${CONSTANT.BASE_URL}/v1/api/user/update`,
      form,
      { headers: this.getHeaders() }
    );
  }

  accountSignIn(form: any): Observable<any> {
    return this.http.post<any>(`${CONSTANT.BASE_URL}/anonymous/signup`, form, {
      headers: this.getHeaders(),
    });
  }
  accountSignIn2(form: any): Observable<any> {
    return this.http.post<any>(
      `${CONSTANT.BASE_URL}/v1/api/user/create`,
      form,
      { headers: this.getHeaders() }
    );
  }

  //Get doctor and patient in Quan ly account
  getDoctorByVillageId(params): Observable<UserObj> {
    return this.http.get<UserObj>(
      `${CONSTANT.BASE_URL}/v1/api/user/get-doctor?villageId=${params}`,
      { headers: this.getHeaders() }
    );
  }
  getDoctorByVillageId2(params): Observable<ListOfUserResponse> {
    return this.http.get<ListOfUserResponse>(
      `${CONSTANT.BASE_URL}/v1/api/patient-doctor/get-doctor-by-village?villageId=${params}`,
      { headers: this.getHeaders() }
    );
  }
  getPatientByVillageId(params): Observable<ListOfUserResponse> {
    return this.http.get<ListOfUserResponse>(
      `${CONSTANT.BASE_URL}/v1/api/user/get-patient?villageId=${params}`,
      { headers: this.getHeaders() }
    );
  }
  updateWorkAssign(form): Observable<any> {
    return this.http.post<any>(
      `${CONSTANT.BASE_URL}/v1/api/patient-doctor/assign`,
      form,
      { headers: this.getHeaders() }
    );
  }
  editWorkAssign(form): Observable<UserResponse> {
    return this.http.post<UserResponse>(
      `${CONSTANT.BASE_URL}/v1/api/patient-doctor/update`,
      form,
      { headers: this.getHeaders() }
    );
  }
  getDeactivePatient(params): Observable<ListOfUserResponse> {
    return this.http.get<ListOfUserResponse>(
      `${CONSTANT.BASE_URL}/v1/api/user/get-patient-deactive?villageId=${params}`,
      { headers: this.getHeaders() }
    );
  }
  getDeactiveDoctor(params): Observable<ListOfUserResponse> {
    return this.http.get<ListOfUserResponse>(
      `${CONSTANT.BASE_URL}/v1/api/user/get-doctor-deactive?villageId=${params}`,
      { headers: this.getHeaders() }
    );
  }
}
