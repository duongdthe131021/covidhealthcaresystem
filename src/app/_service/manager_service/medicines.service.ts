import { CONSTANT } from "app/core/constants";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { TokenStorageService } from "../JWT_service/token-storage.service";
import { MedicineObject, MedicineResponse, MedicinesResponse } from "app/core/models/medicine";

@Injectable({
  providedIn: "root",
})
export class MedicinesService {
  getHeaders = () =>
    new HttpHeaders({
      Authorization: `Bearer ${this.TokenStorageService.getToken()}`,
    });

  constructor(
    private http: HttpClient,
    private TokenStorageService: TokenStorageService
  ) {}

  // Medicine service
  getMedicine(): Observable<MedicinesResponse> {
    return this.http.get<MedicinesResponse>(
      `${CONSTANT.BASE_URL}/v1/api/medicine/get-all`,
      { headers: this.getHeaders() }
    );
  }
  getMedicineById(params): Observable<MedicineObject> {
    const medicineObj = this.http.get<MedicineObject>(
      `${CONSTANT.BASE_URL}/v1/api/medicine/get?medicineId=${params}`,
      { headers: this.getHeaders() }
    );
    return medicineObj;
  }
  medicineUpdate(form: any): Observable<MedicineResponse> {
    const medicineObj = this.http.post<MedicineResponse>(
      `${CONSTANT.BASE_URL}/v1/api/medicine/update`,
      form,
      { headers: this.getHeaders() }
    );
    return medicineObj;
  }
  medicineCreate(form: any): Observable<MedicineResponse> {
    return this.http.post<MedicineResponse>(
      `${CONSTANT.BASE_URL}/v1/api/medicine/create`,
      form,
      { headers: this.getHeaders() }
    );
  }
  deleteMedicineById(params): Observable<any> {
    return this.http.put<any>(
      `${CONSTANT.BASE_URL}/v1/api/medicine/delete?medicineId=${params}`,
      { headers: this.getHeaders() }
    );
  }
}
