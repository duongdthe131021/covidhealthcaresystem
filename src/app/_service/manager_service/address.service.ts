import { DistrictResponse, DistrictsResponse } from './../../core/models/address/district';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CONSTANT } from 'app/core/constants';
import { Observable } from 'rxjs';
import { TokenStorageService } from '../JWT_service/token-storage.service';
import { ProvinceResponse, ProvincesResponse } from 'app/core/models/address/province';
import { VillagesResponse } from 'app/core/models/address/village';

@Injectable({
  providedIn: 'root'
})
export class AddressService {

  getHeaders = () =>
    new HttpHeaders({
      Authorization: `Bearer ${this.TokenStorageService.getToken()}`,
    });

  constructor(
    private http: HttpClient,
    private TokenStorageService: TokenStorageService
  ) { }

  getProvince(): Observable<ProvincesResponse> {
    return this.http.get<ProvincesResponse>(
      `${CONSTANT.BASE_URL}/v1/api/province/get-all`,
      { headers: this.getHeaders() }
    );
  }
  getDistrict(params): Observable<DistrictsResponse> {
    return this.http.get<DistrictsResponse>(
      `${CONSTANT.BASE_URL}/v1/api/district/get-all?provinceId=${params}`,
      { headers: this.getHeaders() }
    );
  }
  getVillage(params): Observable<VillagesResponse> {
    return this.http.get<VillagesResponse>(
      `${CONSTANT.BASE_URL}/v1/api/village/get-all?districtId=${params}`,
      { headers: this.getHeaders() }
    );
  }
}
