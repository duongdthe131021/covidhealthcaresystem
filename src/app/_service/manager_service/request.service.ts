import { ListOfRequest, RequestType } from "./../../core/models/request";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { CONSTANT } from "app/core/constants";
import { Observable } from "rxjs";
import { TokenStorageService } from "../JWT_service/token-storage.service";
@Injectable({
  providedIn: "root",
})
export class RequestService {
  getHeaders = () =>
    new HttpHeaders({
      Authorization: `Bearer ${this.TokenStorageService.getToken()}`,
    });

  constructor(
    private http: HttpClient,
    private TokenStorageService: TokenStorageService
  ) {}

  getRequestByVillageId(params): Observable<ListOfRequest> {
    return this.http.post<ListOfRequest>(
      `${CONSTANT.BASE_URL}/v1/api/request/get-by-village/${params}`,
      {},
      { headers: this.getHeaders() }
    );
  }

  getRequestByStatus(status, villageId): Observable<ListOfRequest> {
    return this.http.post<ListOfRequest>(
      `${CONSTANT.BASE_URL}/v1/api/request/get/status?status=${status}&villageId=${villageId}`,
      {},
      { headers: this.getHeaders() }
    );
  }
  updateRequest(form: any, params): Observable<any> {
    return this.http.post<any>(
      `${CONSTANT.BASE_URL}/v1/api/request/update/${params}`,
      form,
      { headers: this.getHeaders() }
    );
  }
  getRequestType(): Observable<RequestType[]> {
    return this.http.get<RequestType[]>(
      `${CONSTANT.BASE_URL}/v1/api/request/request-type`,
      { headers: this.getHeaders() }
    );
  }
  createRequest(form: any): Observable<any> {
    return this.http.post<any>(
      `${CONSTANT.BASE_URL}/v1/api/request/create`,
      form,
      { headers: this.getHeaders() }
    );
  }
}
