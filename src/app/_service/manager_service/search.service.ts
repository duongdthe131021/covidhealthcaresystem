import { ListOfReport } from "./../../core/models/dailyReport";
import { ListOfResult } from "./../../core/models/test-result";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { CONSTANT } from "app/core/constants";
import { ListOfUser, UserResponse } from "app/core/models/user";
import { BehaviorSubject, Observable } from "rxjs";
import { TokenStorageService } from "../JWT_service/token-storage.service";

@Injectable({
  providedIn: "root",
})
export class SearchService {
  getHeaders = () =>
    new HttpHeaders({
      Authorization: `Bearer ${this.TokenStorageService.getToken()}`,
    });
  constructor(
    private http: HttpClient,
    private TokenStorageService: TokenStorageService
  ) {}
  searchInput$ = new BehaviorSubject<any>({});

  searchUserByUserName(name, villageId): Observable<ListOfUser> {
    return this.http.post<ListOfUser>(
      `${CONSTANT.BASE_URL}/v1/api/user/search?name=${name}&villageId=${villageId}`,
      {},
      { headers: this.getHeaders() }
    );
  }
  searchReportByUserName(name, villageId): Observable<ListOfReport> {
    return this.http.post<ListOfReport>(
      `${CONSTANT.BASE_URL}/v1/api/report/search?name=${name}&villageId=${villageId}`,
      {},
      { headers: this.getHeaders() }
    );
  }
  searchResultByUserName(name, villageId): Observable<ListOfResult> {
    return this.http.post<ListOfResult>(
      `${CONSTANT.BASE_URL}/v1/api/result/search?name=${name}&villageId=${villageId}`,
      {},
      { headers: this.getHeaders() }
    );
  }
}
