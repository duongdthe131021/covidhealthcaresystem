import { CONSTANT } from "./../../core/constants";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { TokenStorageService } from "../JWT_service/token-storage.service";

@Injectable({
  providedIn: "root",
})
export class FileManagementService {
  getHeaders = () =>
    new HttpHeaders({
      Authorization: `Bearer ${this.TokenStorageService.getToken()}`,
    });

  constructor(
    private http: HttpClient,
    private TokenStorageService: TokenStorageService
  ) {}

  //import-user
  importFile(file: File): Observable<any> {
    const formData: FormData = new FormData();
    formData.append("file", file);
    return this.http.post<any>(
      `${CONSTANT.BASE_URL}/v1/api/user/upload`,
      formData,
      { headers: this.getHeaders()}
    );
  }
  //Export-user
  exportUserFile(form: any): Observable<any> {
    return this.http.post<any>(
      `${CONSTANT.BASE_URL}/v1/api/user/export/accounts.xlsx`,
      form,
      { headers: this.getHeaders(), responseType: 'blob' as 'json' }
    );
  }

  //Export-report
  exportReportFile(params): Observable<any> {
    return this.http.post<any>(
      `${CONSTANT.BASE_URL}/v1/api/report/export/daily_report.xlsx?userId=${params}`,
      {},
      { headers: this.getHeaders(), responseType: 'blob' as 'json' }
    );
  }

  //import-test-result
  importTestResultFile(file: File): Observable<string> {
    const formData: FormData = new FormData();
    formData.append("file", file);
    return this.http.post<any>(
      `${CONSTANT.BASE_URL}/v1/api/result/upload`,
      formData,
      { headers: this.getHeaders(), responseType: 'string' as 'json'  }
    );
  }

  //Covid-test-result
  exportCovidResultFile(form: any): Observable<any> {
    return this.http.post<any>(
      `${CONSTANT.BASE_URL}/v1/api/result/export/result_test_covid.xlsx`,
      form,
      { headers: this.getHeaders(), responseType: 'blob' as 'json' }
    );
  }
}
