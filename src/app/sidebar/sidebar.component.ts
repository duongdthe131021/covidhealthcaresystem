import { AdminGuardService } from "./../_service/JWT_service/roleBase/admin-guard.service";
import { Component, OnInit, ViewChild } from "@angular/core";
import { TokenStorageService } from "app/_service/JWT_service/token-storage.service";

export interface RouteInfo {
  title: string;
  icon: string;
  iconPlus: string;
  iconMinus?: String;
  class: string;
  isToggle?: boolean;
  child?: any;
  path?: string;
}

@Component({
  moduleId: module.id,
  selector: "sidebar-cmp",
  templateUrl: "sidebar.component.html",
  styleUrls: ["./sidebar.component.css"],
})
export class SidebarComponent implements OnInit {
  public menuItems: any[];

  ROUTES: RouteInfo[] = [];

  isAdmin: boolean;
  constructor(private tokenStorage: TokenStorageService) {
    const role = this.tokenStorage.getRole();
    this.ROUTES = [
      {
        title: "QUản lý tài khoản",
        icon: "assets/images/icon/profile.png",
        iconPlus: "assets/images/icon/plus.png",
        iconMinus: "assets/images/icon/minus.png",
        class: "",
        isToggle: false,
        child: [
          {
            path: "/user-create",
            title: "Thêm mới tài khoản",
            isAdmin:
              ["Super admin", "Trung tâm Y tế", "Trạm Y tế"].indexOf(role) > -1
                ? true
                : false,
          },
          {
            path: "/medical-staff",
            title: "Trạm y tế",
            isAdmin:
              ["Super admin", "Trung tâm Y tế", "Trạm Y tế"].indexOf(role) > -1
                ? true
                : false,
          },
          {
            path: "/user-active",
            title: "Kích hoạt tài khoản",
            isAdmin:
              ["Super admin", "Trung tâm Y tế", "Trạm Y tế"].indexOf(role) > -1
                ? true
                : false,
          },
        ],
      },

      {
        title: "Quản lý báo cáo",
        icon: "assets/images/icon/report.png",
        iconPlus: "assets/images/icon/plus.png",
        iconMinus: "assets/images/icon/minus.png",
        class: "",
        isToggle: false,
        child: [
          {
            path: "/report",
            title: "Danh sách báo cáo",
            isAdmin:
              ["Super admin", "Trung tâm Y tế", "Trạm Y tế"].indexOf(role) > -1
                ? true
                : false,
          },
        ],
      },
      {
        title: "Kết quả xét nghiệm",
        icon: "assets/images/icon/clipart1119055.png",
        iconPlus: "assets/images/icon/plus.png",
        iconMinus: "assets/images/icon/minus.png",
        class: "",
        isToggle: false,
        child: [
          {
            path: "/test-result",
            title: "Danh sách kết quả",
            isAdmin:
              ["Super admin", "Trung tâm Y tế", "Trạm Y tế"].indexOf(role) > -1
                ? true
                : false,
          },
          {
            path: "/test-result-create",
            title: "Thêm mới kết quả",
            isAdmin:
              ["Super admin", "Trung tâm Y tế", "Trạm Y tế"].indexOf(role) > -1
                ? true
                : false,
          },
        ],
      },
      {
        title: "Yêu cầu",
        icon: "assets/images/icon/request_icon.png",
        iconPlus: "assets/images/icon/plus.png",
        iconMinus: "assets/images/icon/minus.png",
        class: "",
        isToggle: false,
        child: [
          {
            path: "/request",
            title: "Danh sách yêu cầu",
            isAdmin:
              ["Super admin", "Trung tâm Y tế", "Trạm Y tế"].indexOf(role) > -1
                ? true
                : false,
          },
          {
            path: "/request-create",
            title: "Gửi yêu cầu",
            isAdmin: ["Trạm Y tế"].indexOf(role) > -1 ? true : false,
          },
        ],
      },
      {
        title: "Quản lý tập luyện",
        icon: "assets/images/icon/exercies.png",
        iconPlus: "assets/images/icon/plus.png",
        iconMinus: "assets/images/icon/minus.png",
        class: "",
        isToggle: false,
        child: [
          {
            path: "/exercise",
            title: "Danh sách bài tập",
            isAdmin:
              ["Super admin", "Trung tâm Y tế", "Trạm Y tế"].indexOf(role) > -1
                ? true
                : false,
          },
          {
            path: "/exercise-create",
            title: "Thêm mới bài tập",
            isAdmin: ["Super admin"].indexOf(role) > -1 ? true : false,
          },
        ],
      },
      {
        title: "Quản lý thuốc",
        icon: "assets/images/icon/medicine.png",
        iconPlus: "assets/images/icon/plus.png",
        iconMinus: "assets/images/icon/minus.png",
        class: "",
        isToggle: false,
        child: [
          {
            path: "/medicine",
            title: "Danh sách thuốc",
            isAdmin:
              ["Super admin", "Trung tâm Y tế", "Trạm Y tế"].indexOf(role) > -1
                ? true
                : false,
          },
          {
            path: "/medicine-create",
            title: "Tạo mới thuốc",
            isAdmin: ["Super admin"].indexOf(role) > -1 ? true : false,
          },
        ],
      },
      {
        title: "Dấu hiệu/triệu chứng",
        icon: "assets/images/icon/heart@4x.png",
        iconPlus: "assets/images/icon/plus.png",
        iconMinus: "assets/images/icon/minus.png",
        class: "",
        isToggle: false,
        child: [
          {
            path: "/symptom",
            title: "Danh sách triệu chứng",
            isAdmin:
              ["Super admin", "Trung tâm Y tế", "Trạm Y tế"].indexOf(role) > -1
                ? true
                : false,
          },
          {
            path: "/symptom-create",
            title: "Thêm mới triệu chứng",
            isAdmin: ["Super admin"].indexOf(role) > -1 ? true : false,
          },
        ],
      },
    ];
  }

  ngOnInit() {
    this.menuItems = this.ROUTES.filter((menuItem) => menuItem);
    // this.adminGuard();
    // console.log( this.adminGuard());
  }

  onToggleDropdown(index, toggle) {
    this.menuItems[index].isToggle = !toggle;
  }
}
