import { FormBuilder } from "@angular/forms";
import { FormGroup } from "@angular/forms";
import {
  Component,
  OnInit,
  Renderer2,
  ViewChild,
  ElementRef,
} from "@angular/core";
// import { ROUTES } from "../../sidebar/sidebar.component";
import { Router } from "@angular/router";
import { Location } from "@angular/common";
import { TokenStorageService } from "app/_service/JWT_service/token-storage.service";
import { UserResponse } from "app/core/models/user";
import { SearchService } from "app/_service/manager_service/search.service";

@Component({
  moduleId: module.id,
  selector: "navbar-cmp",
  templateUrl: "navbar.component.html",
})
export class NavbarComponent implements OnInit {
  private listTitles: any[];
  location: Location;
  private nativeElement: Node;
  private toggleButton;
  private sidebarVisible: boolean;
  private searchResult: String;
  isSearch = false;
  public isCollapsed = true;
  private titleName: string;
  searchForm: FormGroup;

  isLoginPage = false;
  currentUser: UserResponse;

  @ViewChild("navbar-cmp", { static: false }) button;

  constructor(
    location: Location,
    private renderer: Renderer2,
    private element: ElementRef,
    private router: Router,
    private tokenStorageService: TokenStorageService,
    private fb: FormBuilder,
    private searchService: SearchService
  ) {
    this.location = location;
    this.nativeElement = element.nativeElement;
    this.sidebarVisible = false;
    this.currentUser = this.tokenStorageService.getUser();
    this.searchForm = this.fb.group({
      name: "",
    });
  }

  ngOnInit() {
    // this.listTitles = ROUTES.filter((listTitle) => listTitle);
    var navbar: HTMLElement = this.element.nativeElement;
    this.toggleButton = navbar.getElementsByClassName("navbar-toggle")[0];
    this.router.events.subscribe((event) => {
      this.sidebarClose();
    });

    // for (var item = 0; item < this.pathList.length; item++) {
    //   if (this.pathList[item] === this.router.url) {
    //     this.isSearch = true;
    //   }
    // }

    this.tokenStorageService.isLogin$.subscribe((data) => {
      this.currentUser = this.tokenStorageService.getUser();
    });
  }
  onNameSubmit() {
    const queryParams = {
      url: this.router.url,
      name: this.searchForm.get("name").value,
    };
    this.searchService.searchInput$.next(queryParams);
    this.router.navigate(["/search"], {
      queryParams: { name: this.searchForm.get("name").value },
    });
  }
  getFullName(lastName, surName, firstName) {
    return lastName + " " + surName + " " + firstName;
  }
  getTitle() {
    switch (this.router.url.split("?")[0]) {
      case "/user":
        return (this.titleName = "Trang cá nhân");
      case "/user-profile":
        return (this.titleName = "Tài khoản người dùng");
      case "/medical-staff":
        return (this.titleName = "Quản lý trạm y tế");
      case "/user-active":
        return (this.titleName = "kích hoạt tài khoản");
      case "/patient":
        return (this.titleName = "Quản lý tài khoản");
      case "/assign":
        return (this.titleName = "Chỉ định bác sĩ");
      case "/assign-update":
        return (this.titleName = "Quản lý trạm y tế");
      case "/user-create":
        return (this.titleName = "Tạo tài khoản");
      case "/report":
        return (this.titleName = "Quản lý báo cáo");
      case "/report-detail":
        return (this.titleName = "Quản lý báo cáo");
      case "/exercise":
        return (this.titleName = "Quản lý bài tập");
      case "/exercise-detail":
        return (this.titleName = "Quản lý bài tập");
      case "/exercise-create":
        return (this.titleName = "Tạo mới bài tập");
      case "/exercise-edit":
        return (this.titleName = "Cập nhật bài tập");
      case "/medicine":
        return (this.titleName = "Quản lý thuốc");
      case "/medicine-detail":
        return (this.titleName = "Quản lý thuốc");
      case "/medicine-edit":
        return (this.titleName = "Cập nhật thuốc");
      case "/medicine-create":
        return (this.titleName = "Tạo mới thuốc");
      case "/symptom":
        return (this.titleName = "Quản lý triệu chứng");
      case "/symptom-detail":
        return (this.titleName = "Quản lý triệu chứng");
      case "/symptom-edit":
        return (this.titleName = "Cập nhật triệu chứng");
      case "/symptom-create":
        return (this.titleName = "Tạo mới triệu chứng");
      case "/test-result":
        return (this.titleName = "Quản lý xét nghiệm");
      case "/test-result-create":
        return (this.titleName = "Tạo mới xét nghiệm");
      case "/test-result-detail":
        return (this.titleName = "Quản lý xét nghiệm");
      case "/request":
        return (this.titleName = "Quản lý yêu cầu");
        case "/request-create":
        return (this.titleName = "Gửi yêu cầu");
      case "/password-change":
        return (this.titleName = "Đổi mật khẩu");
      case "/search":
        return (this.titleName = "Tìm kiếm");
      default:
        return (this.titleName = "Bảng điều khiển");
    }
  }
  sidebarToggle() {
    if (this.sidebarVisible === false) {
      this.sidebarOpen();
    } else {
      this.sidebarClose();
    }
  }
  sidebarOpen() {
    const toggleButton = this.toggleButton;
    const html = document.getElementsByTagName("html")[0];
    const mainPanel = <HTMLElement>(
      document.getElementsByClassName("main-panel")[0]
    );
    setTimeout(function () {
      toggleButton.classList.add("toggled");
    }, 500);

    html.classList.add("nav-open");
    if (window.innerWidth < 991) {
      mainPanel.style.position = "fixed";
    }
    this.sidebarVisible = true;
  }
  sidebarClose() {
    const html = document.getElementsByTagName("html")[0];
    const mainPanel = <HTMLElement>(
      document.getElementsByClassName("main-panel")[0]
    );
    if (window.innerWidth < 991) {
      setTimeout(function () {
        mainPanel.style.position = "";
      }, 500);
    }
    this.toggleButton.classList.remove("toggled");
    this.sidebarVisible = false;
    html.classList.remove("nav-open");
  }
  collapse() {
    this.isCollapsed = !this.isCollapsed;
    const navbar = document.getElementsByTagName("nav")[0];
    console.log(navbar);
    if (!this.isCollapsed) {
      navbar.classList.remove("navbar-transparent");
      navbar.classList.add("bg-white");
    } else {
      navbar.classList.add("navbar-transparent");
      navbar.classList.remove("bg-white");
    }
  }
  logout(): void {
    this.tokenStorageService.signOut();
    this.router.navigate(["/login"]);
  }

  ngOnDestroy(): void {
    this.tokenStorageService.isLogin$.next(false);
  }
}
