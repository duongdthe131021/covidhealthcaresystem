import { Observable, Subscriber } from "rxjs";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Component, OnInit, ViewChild } from "@angular/core";
import { TokenStorageService } from "app/_service/JWT_service/token-storage.service";
import { MatSnackBar } from "@angular/material/snack-bar";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { UserServiceService } from "app/_service/JWT_service/user-service.service";
import { User } from "app/core/models/user";
import { formatDate } from "@angular/common";

@Component({
  selector: "user-cmp",
  moduleId: module.id,
  templateUrl: "user.component.html",
})
export class UserComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  @ViewChild("file") file: any;
  formProfile: FormGroup;
  currentUser: User;

  backgroundImg = "assets/img/damir-bosnjak.jpg";
  avatar = "assets/images/default-avatar.jpg";
  baseImg =
    "https://healthcaresystemstorage.s3.us-east-2.amazonaws.com/images/";
  uploadedImg: string;
  fullName: string;
  address: string;
  dob: Date;
  lastName: string;
  firstName: string;
  surName: string;

  constructor(
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private tokenStorageService: TokenStorageService,
    private userService: UserServiceService
  ) {
    this.currentUser = this.tokenStorageService.getUser();
    if (this.currentUser.avatar != null) {
      this.avatar = this.baseImg + this.currentUser.avatar;
    }
    this.fullName = this.getFullName(
      this.currentUser.lastname,
      this.currentUser.surname,
      this.currentUser.firstname
    );
    this.address = this.getFullAddress(
      this.currentUser.villageName,
      this.currentUser.districtName,
      this.currentUser.provinceName
    );

    this.formProfile = this.formBuilder.group({
      userId: [this.currentUser.userId, Validators.required],
      username: [this.currentUser.email, Validators.required],
      gender: [this.currentUser.gender],
      isOnline: [1],
      isActive: this.currentUser.isActive,
      fullName: [this.fullName],
      firstname: [this.currentUser.firstname],
      surname: [this.currentUser.surname],
      lastname: [this.currentUser.lastname],
      dob: [this.currentUser.dateOfBirth],
      mail: [
        this.currentUser.email,
        Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$"),
      ],
      phone: [
        this.currentUser.phone,
        Validators.pattern("[0-9]{10}"),
      ],
      avatar: [this.currentUser.avatar],
      identityCard: null,
      address: [this.currentUser.address],
      villageId: [this.currentUser.villageId]
    });
  }

  ngOnInit() {}

  onSubmit() {
    console.log(this.formProfile.value);
    if (this.formProfile.invalid) {
      this.blockUI.stop();
      this.snackBar.open("Thiếu thông tin", "Ẩn", {
        duration: 3000,
        horizontalPosition: "right",
        verticalPosition: "bottom",
      });
      return;
    } else {
      this.blockUI.start();
      this.userService.userUpdate(this.formProfile.value).subscribe(
        (data) => {
          this.blockUI.stop();
          if (data.message === "Update user success") {
            this.userService.getCurrentUser().subscribe((data) => {
              if (data) {
                this.tokenStorageService.saveUser(data);
              }
            });
            this.snackBar.open("Cập nhật thành công", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          } 
          else if (data.code === "PHONENUMBER_EXISTED"){
            this.snackBar.open("Số điện thoại đã tồn tại", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          } else{
            this.snackBar.open("Cập nhật thất bại", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          }
        },
        (error) => {
          this.blockUI.stop();
          this.snackBar.open("Cập nhật thất bại", "Ẩn", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "bottom",
          });
        }
      );
    }
  }

  onSelectFile(event) {
    const file = event.target.files[0];
    this.convertToBase64(file);
  }
  convertToBase64(file: File) {
    const observable = new Observable((subscriber: Subscriber<any>) => {
      this.readFile(file, subscriber);
    });
    observable.subscribe(
      (data) => {
        this.avatar = data;
        this.formProfile.patchValue({
          avatar: data,
        });
      },
      (error) => {
        alert(error);
      }
    );
  }

  readFile(file: File, subscriber: Subscriber<any>) {
    const fileReader = new FileReader();
    fileReader.readAsDataURL(file);

    fileReader.onload = () => {
      subscriber.next(fileReader.result);
      subscriber.complete();
    };
    fileReader.onerror = (error) => {
      subscriber.error(error);
      subscriber.complete();
    };
  }
  convertFullName(name) {
    var names = name.split(" ");
    this.lastName = names[0].toString();
    this.firstName = names[names.length - 1].toString();
    this.surName ="";
    for (var i = 1; i != names.length; i++) {
      if (i != names.length - 1) {
        if (i == 1) {
          this.surName = this.surName + names[i];
        } else {
          this.surName = this.surName + " " + names[i];
        }
      }
    }
    this.formProfile.patchValue({
      firstname: this.firstName,
      surname: this.surName,
      lastname: this.lastName,
    });
  }
  getFullName(lastName, surName, firstName) {
    return lastName + " " + surName + " " + firstName;
  }
  getFullAddress(villageName, districtName, provinceName) {
    return villageName + " - " + districtName + " - " + provinceName;
  }
}
