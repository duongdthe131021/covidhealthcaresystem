import { RequestType } from "./../../../core/models/request";
import { FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { RequestService } from "./../../../_service/manager_service/request.service";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { Component, OnInit } from "@angular/core";
import { User } from "app/core/models/user";
import { TokenStorageService } from "app/_service/JWT_service/token-storage.service";
import { MatSnackBar } from "@angular/material/snack-bar";
import { FormBuilder } from "@angular/forms";
import * as moment from "moment";

@Component({
  selector: "app-request-create",
  templateUrl: "./request-create.component.html",
  styleUrls: ["./request-create.component.css"],
})
export class RequestCreateComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  formCreate: FormGroup;
  currentUser: User;

  villageId: number;
  requestTypes: RequestType[];
  isSuperAdmin = false;
  isAdmin = false;
  isMod = false;
  currentDate = new Date();
  selectedTypeId: number

  constructor(
    private tokenStorageService: TokenStorageService,
    private requestService: RequestService,
    private snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
    const formatDate = moment(this.currentDate).format("YYYY-MM-DD");
    this.formCreate = this.formBuilder.group({
      date: [formatDate, Validators.required],
      description: [null],
      requestTypeId: [null, Validators.required],
    });
  }

  ngOnInit(): void {
    this.currentUser = this.tokenStorageService.getUser();
    if (
      this.currentUser.roleName === "Super admin" ||
      this.currentUser.roleName === "Trung tâm Y tế"
    ) {
      this.isMod = true;
      this.router.navigate(["/dashboard"]);
    }
    this.getRequestType();
  }
  onChangeRequest(value) {
    this.selectedTypeId = value;
    this.formCreate.patchValue({ requestTypeId: value });
  }

  getRequestType() {
    this.blockUI.start();
    this.requestService.getRequestType().subscribe(
      (data) => {
        if (data) {
          this.blockUI.stop();
          this.requestTypes = data;
        } else {
          this.blockUI.stop();
          this.snackBar.open("Tải trang lỗi", "Ẩn", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "bottom",
          });
        }
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }
  onCreate() {
    console.log(this.formCreate.value);
    if (this.formCreate.invalid) {
      this.snackBar.open("Chưa đủ hoặc sai thông tin", "Ẩn", {
        duration: 3000,
        horizontalPosition: "right",
        verticalPosition: "bottom",
      });
      return;
    } else {
      this.blockUI.start();
      this.requestService.createRequest(this.formCreate.value).subscribe(
        (res) => {
          this.blockUI.stop();
          this.formCreate.reset();
          this.formCreate.patchValue({
            requestTypeId: this.selectedTypeId,
            date: moment(this.currentDate).format("YYYY-MM-DD")
          });
          this.snackBar.open("Gửi yêu cầu thành công", "Ẩn", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "bottom",
          });
        },
        (err) => {
          this.blockUI.stop();
          this.snackBar.open("Gửi yêu cầu thất bại", "Ẩn", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "bottom",
          });
        }
      );
    }
  }
  onCancel() {
    this.formCreate.reset();
    this.formCreate.patchValue({
      requestTypeId: this.selectedTypeId,
      date: moment(this.currentDate).format("YYYY-MM-DD")
    });
  }
}
