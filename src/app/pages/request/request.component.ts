import { MatDialog } from "@angular/material/dialog";
import { ListOfProvince } from "app/core/models/address/province";
import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatSnackBar } from "@angular/material/snack-bar";
import { MatTabChangeEvent } from "@angular/material/tabs";
import { ListOfRequest } from "app/core/models/request";
import { ListOfUser, User } from "app/core/models/user";
import { TokenStorageService } from "app/_service/JWT_service/token-storage.service";
import { AccountService } from "app/_service/manager_service/account.service";
import { RequestService } from "app/_service/manager_service/request.service";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { ListOfDistrict } from "app/core/models/address/district";
import { AddressService } from "app/_service/manager_service/address.service";
import { RequestResponseComponent } from "../dialog/request-response/request-response.component";

@Component({
  selector: "app-request",
  templateUrl: "./request.component.html",
  styleUrls: ["./request.component.css"],
})
export class RequestComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  p: number = 1;

  currentUser: User;
  listOfProvince: ListOfProvince;
  listOfDistrict: ListOfDistrict;
  listOfMedicalStaff: ListOfUser;
  listOfRequest: ListOfRequest;
  selectedProvince: number;
  selectedDistrict: number;
  selectedMedicalStaff: number;

  isSuperAdmin = false;
  isAdmin = false;
  isMod = false;
  villageId: number;
  defaultTab = 2;
  currentTab = 2;

  defaultAvatar = "assets/images/default-avatar.jpg";
  baseImg =
    "https://healthcaresystemstorage.s3.us-east-2.amazonaws.com/images/";

  constructor(
    private requestService: RequestService,
    private accountService: AccountService,
    private addressService: AddressService,
    private snackBar: MatSnackBar,
    private tokenStorageService: TokenStorageService,
    private formBuilder: FormBuilder,
    private matDialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.currentUser = this.tokenStorageService.getUser();
    if (this.currentUser.roleName === "Super admin") {
      this.isSuperAdmin = true;
      this.getProvince();
    } else if (this.currentUser.roleName === "Trung tâm Y tế") {
      this.isAdmin = true;
      this.getMedicalStaffByAdmin(this.currentUser.districtId);
    } else {
      this.isMod = true;
      this.villageId = parseInt(this.currentUser.villageId);
      this.getRequestByVillageId(this.defaultTab, this.villageId);
    }
  }

  //superAdmin
  getProvince() {
    this.blockUI.start();
    this.addressService.getProvince().subscribe(
      (data) => {
        this.blockUI.stop();
        this.listOfProvince = data.data;
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }
  onChangeProvinceId(value) {
    this.selectedProvince = value;
    this.listOfDistrict = null;
    this.listOfMedicalStaff = null;
    this.listOfRequest = null;
    this.getDistrict(value);
  }
  getDistrict(provinceId) {
    this.blockUI.start();
    this.addressService.getDistrict(provinceId).subscribe(
      (data) => {
        this.blockUI.stop();
        this.listOfDistrict = data.data;
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }
  onChangeDistrictId(value) {
    this.selectedDistrict = value;
    this.listOfMedicalStaff = null;
    this.listOfRequest = null;
    this.getMedicalStaff(value);
  }
  getMedicalStaff(districtId) {
    this.blockUI.start();
    this.accountService.getMedicalStaff(districtId).subscribe(
      (data) => {
        this.blockUI.stop();
        if (data.data.length != 0) {
          this.listOfMedicalStaff = data.data;
        } else {
          this.blockUI.stop();
          this.snackBar.open("Chưa có trạm y tế nào", "Ẩn", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "bottom",
          });
        }
      }
      // (err) => {
      //   this.blockUI.stop();
      //   this.snackBar.open("Lỗi hệ thống", "Ẩn", {
      //     duration: 3000,
      //     horizontalPosition: "right",
      //     verticalPosition: "bottom",
      //   });
      // }
    );
  }
  onChangeVillage(villageValue) {
    this.villageId = villageValue;
    this.getRequestByVillageId(this.currentTab, this.villageId);
  }

  //admin
  public tabChanged(tabChangeEvent: MatTabChangeEvent): void {
    this.currentTab = tabChangeEvent.index + 1;
    if(this.isSuperAdmin){
      this.getRequestByVillageId(this.currentTab, this.villageId);
    }
    else if (this.isAdmin) {
      this.getRequestByVillageId(this.currentTab, this.villageId);
    } else {
      this.getRequestByVillageId(this.currentTab, this.currentUser.villageId);
    }
  }
  getMedicalStaffByAdmin(districtId) {
    this.blockUI.start();
    this.listOfMedicalStaff = [];
    this.accountService.getMedicalStaff(districtId).subscribe(
      (data) => {
        if (data.message.toLowerCase() == "success") {
          this.listOfMedicalStaff = data.data;
          this.villageId = this.listOfMedicalStaff[0].village.villageId;
          this.getRequestByVillageId(2, this.villageId);
          this.blockUI.stop();
        } else {
          this.blockUI.stop();
          this.snackBar.open("Tải trang lỗi", "Ẩn", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "bottom",
          });
        }
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }
  //end of admin

  getRequestByVillageId(requestStatus, villageId) {
    this.blockUI.start();
    this.listOfRequest = [];
    this.requestService.getRequestByStatus(requestStatus, villageId).subscribe(
      (data) => {
        if (data) {
          this.listOfRequest = data;
          this.blockUI.stop();
        } else {
          this.blockUI.stop();
          this.snackBar.open("Tải trang lỗi", "Ẩn", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "bottom",
          });
        }
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }

  onAprove(requestId) {
    const dialogRef = this.matDialog.open(RequestResponseComponent, {
      data: {
        status: 1,
        requestId: requestId,
      },
    });
    dialogRef.afterClosed().subscribe((data) => {
      this.getRequestByVillageId(2, this.villageId);
    });
  }
  onReject(requestId) {
    const dialogRef = this.matDialog.open(RequestResponseComponent, {
      data: {
        status: 3,
        requestId: requestId,
        villageId: this.villageId,
      },
    });
    dialogRef.afterClosed().subscribe((data) => {
      this.getRequestByVillageId(2, this.villageId);
    });
  }

  getFullName(lastName, surName, firstName) {
    return lastName + " " + surName + " " + firstName;
  }
  getDescription(description) {
    if (description == null || description == "") {
      return "không có mô tả";
    } else {
      return description;
    }
  }
}
