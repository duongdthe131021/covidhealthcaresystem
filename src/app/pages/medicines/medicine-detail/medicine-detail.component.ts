import { BlockUI, NgBlockUI } from "ng-block-ui";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Medicine } from "app/core/models/medicine";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MedicinesService } from "app/_service/manager_service/medicines.service";
import { TokenStorageService } from "app/_service/JWT_service/token-storage.service";
import { User } from "app/core/models/user";

@Component({
  selector: "app-medicine-detail",
  templateUrl: "./medicine-detail.component.html",
  styleUrls: ["./medicine-detail.component.css"],
})
export class MedicineDetailComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;

  medicine: Medicine;
  id: string;
  defaultImg = "/assets/images/default-medicine-image.jpg";
  baseImg =
    "https://healthcaresystemstorage.s3.us-east-2.amazonaws.com/images/";
  currentUser: User;
  isSuperAdmin = false;
  isAdmin = false;
  constructor(
    private tokenStorageService: TokenStorageService,
    private snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private service: MedicinesService,
    private router: Router
  ) {
    this.route.queryParams.subscribe((params) => {
      this.id = params.id;
    });
  }

  ngOnInit(): void {
    this.currentUser = this.tokenStorageService.getUser();
    if (this.currentUser.roleName === "Super admin") {
      this.isSuperAdmin = true;
    } else if (this.currentUser.roleName === "Trung tâm Y tế") {
      this.isAdmin = true;
    }
    this.getMedicineById();
  }

  getMedicineById() {
    this.blockUI.start();
    this.service.getMedicineById(this.id).subscribe(
      (data) => {
        this.blockUI.stop();
        this.medicine = data.data;
        if (this.medicine.thumbnail != "" || this.medicine.thumbnail != null) {
          this.defaultImg = this.baseImg + this.medicine.thumbnail;
        }
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Tải trang lỗi", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }

  onEditBtn(id: string) {
    this.router.navigate(["/medicine-edit"], { queryParams: { id: id } });
  }
}
