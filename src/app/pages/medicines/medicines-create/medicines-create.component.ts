import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Router } from "@angular/router";
import { User } from "app/core/models/user";
import { TokenStorageService } from "app/_service/JWT_service/token-storage.service";
import { MedicinesService } from "app/_service/manager_service/medicines.service";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { Observable, Subscriber } from "rxjs";

@Component({
  selector: "app-medicines-create",
  templateUrl: "./medicines-create.component.html",
  styleUrls: ["./medicines-create.component.css"],
})
export class MedicinesCreateComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  formCreate: FormGroup;

  defaultImg = "/assets/images/default-medicine-image.jpg";
  baseImg =
    "https://healthcaresystemstorage.s3.us-east-2.amazonaws.com/images/";

  currentUser: User;

  constructor(
    private tokenStorageService: TokenStorageService,
    private router: Router,
    private formBuilder: FormBuilder,
    private medicineService: MedicinesService,
    private snackBar: MatSnackBar
  ) {
    this.currentUser = this.tokenStorageService.getUser();
    if (
      this.currentUser.roleName === "Trung tâm Y tế" ||
      this.currentUser.roleName === "Trạm Y tế"
    ) {
      this.router.navigate(["/dashboard"]);
    }
  }

  ngOnInit(): void {
    this.initFormCreate();
  }
  initFormCreate() {
    this.formCreate = this.formBuilder.group({
      name: ["", [Validators.required, Validators.minLength(3)]],
      thumbnail: [null, Validators.required],
      description: ["", Validators.required],
      detail: ["", Validators.required],
    });
  }
  onSubmit() {
    if (this.formCreate.invalid) {
      this.snackBar.open("Chưa nhập đủ thông tin", "Ẩn", {
        duration: 3000,
        horizontalPosition: "right",
        verticalPosition: "bottom",
      });
      return;
    } else {
      this.blockUI.start();
      this.medicineService.medicineCreate(this.formCreate.value).subscribe(
        (response) => {
          this.blockUI.stop();
          if (response.code == "SUCCESS") {
            this.formCreate.reset();
            this.defaultImg = "/assets/images/default-medicine-image.jpg";
            this.snackBar.open("Tạo mới thành công", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          } else {
            this.blockUI.stop();
            this.snackBar.open("Tạo mới thất bại", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          }
        },
        (err) => {
          this.blockUI.stop();
          this.snackBar.open("Lỗi hệ thống", "Ẩn", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "bottom",
          });
        }
      );
    }
  }
  onSelectFile(event) {
    const file = event.target.files[0];
    this.convertToBase64(file);
  }
  convertToBase64(file: File) {
    const observable = new Observable((subscriber: Subscriber<any>) => {
      this.readFile(file, subscriber);
    });
    observable.subscribe(
      (data) => {
        this.defaultImg = data;
        this.formCreate.patchValue({
          thumbnail: data,
        });
      },
      (error) => {
        alert(error);
      }
    );
  }

  readFile(file: File, subscriber: Subscriber<any>) {
    const fileReader = new FileReader();
    fileReader.readAsDataURL(file);

    fileReader.onload = () => {
      subscriber.next(fileReader.result);
      subscriber.complete();
    };
    fileReader.onerror = (error) => {
      subscriber.error(error);
      subscriber.complete();
    };
  }
  onCancel() {
    this.formCreate.reset();
    this.defaultImg = "/assets/images/default-medicine-image.jpg";
  }
}
