import { Medicine } from "./../../../core/models/medicine";
import { ActivatedRoute, Router } from "@angular/router";
import { Component, OnInit } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { MedicinesService } from "app/_service/manager_service/medicines.service";
import { Observable, Subscriber } from "rxjs";
import { User } from "app/core/models/user";
import { TokenStorageService } from "app/_service/JWT_service/token-storage.service";

@Component({
  selector: "app-medicine-edit",
  templateUrl: "./medicine-edit.component.html",
  styleUrls: ["./medicine-edit.component.css"],
})
export class MedicineEditComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  id: string;
  formUpdate: FormGroup;
  medicine: Medicine;
  defaultImg = "/assets/images/default-medicine-image.jpg";
  baseImg =
    "https://healthcaresystemstorage.s3.us-east-2.amazonaws.com/images/";
    currentUser: User;

  constructor(
    private tokenStorageService: TokenStorageService,
    private router: Router,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private service: MedicinesService,
  ) {
    this.route.queryParams.subscribe((params) => {
      this.id = params.id;
    });
    this.currentUser = this.tokenStorageService.getUser();
    if (
      this.currentUser.roleName === "Trung tâm Y tế" ||
      this.currentUser.roleName === "Trạm Y tế"
    ) {
      this.router.navigate(["/dashboard"]);
    }
  }

  ngOnInit(): void {
    this.getMedicineById();
    this.initForm();
  }
  initForm(medicine?) {
    this.formUpdate = this.formBuilder.group({
      id: medicine?.id,
      name: [medicine?.name, Validators.required],
      detail: [medicine?.detail, Validators.required],
      description: [medicine?.description, Validators.required],
      thumbnail: [medicine?.thumbnail],
      isActive: [medicine?.isActive, Validators.required],
    });
  }
  getMedicineById() {
    this.blockUI.start();
    this.service.getMedicineById(this.id).subscribe(
      (data) => {
        this.blockUI.stop();
        this.medicine = data.data;
        this.initForm(this.medicine);
        if (this.medicine.thumbnail != "" || this.medicine.thumbnail != null) {
          this.defaultImg = this.baseImg + this.medicine.thumbnail;
        }
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }

  onSubmit() {
    if (this.formUpdate.invalid) {
      this.snackBar.open("Chưa đủ thông tin", "Ẩn", {
        duration: 3000,
        horizontalPosition: "right",
        verticalPosition: "bottom",
      });
      this.blockUI.stop();
      return;
    } else {
      this.blockUI.start();
      this.service.medicineUpdate(this.formUpdate.value).subscribe(
        (res) => {
          this.blockUI.stop();
          if (res.code == "UPDATE_MEDICINE_SUCCESS") {
            this.snackBar.open("Cập nhật thành công", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
            this.router.navigate(["/medicine-detail"], {
              queryParams: { id: this.id },
            });
          } else if (res.code == "MEDICINE_NAME_EXIST") {
            this.snackBar.open("Đã trùng tên thuốc", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });            
          }
           else{
            this.snackBar.open("Cập nhật thất bại", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          }
        },
        (error) => {
          this.blockUI.stop();
          this.snackBar.open("Lỗi hệ thống", "Ẩn", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "bottom",
          });
        }
      );
    }
  }

  onSelectFile(event) {
    const file = event.target.files[0];
    this.convertToBase64(file);
  }
  convertToBase64(file: File) {
    const observable = new Observable((subscriber: Subscriber<any>) => {
      this.readFile(file, subscriber);
    });
    observable.subscribe(
      (data) => {
        this.defaultImg = data;
        this.formUpdate.patchValue({
          thumbnail: data,
        });
      },
      (error) => {
        alert(error);
      }
    );
  }

  readFile(file: File, subscriber: Subscriber<any>) {
    const fileReader = new FileReader();
    fileReader.readAsDataURL(file);

    fileReader.onload = () => {
      subscriber.next(fileReader.result);
      subscriber.complete();
    };
    fileReader.onerror = (error) => {
      subscriber.error(error);
      subscriber.complete();
    };
  }

  onCancel() {
    this.router.navigate(["/medicine-detail"], {
      queryParams: { id: this.id },
    });
  }
}
