import { PermissionBlockComponent } from "./../errors/permission-block/permission-block.component";
import { MatDialog } from "@angular/material/dialog";
import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { TokenStorageService } from "app/_service/JWT_service/token-storage.service";
import { AuthServiceService } from "app/_service/JWT_service/auth-service.service";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { UserServiceService } from "app/_service/JWT_service/user-service.service";
import { MatSnackBar } from "@angular/material/snack-bar";
import { UserResponse } from "app/core/models/user";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
})
export class LoginComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage: string = "";
  roles: string[] = [];
  formLogin: FormGroup;
  currentUser: UserResponse;
  password: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthServiceService,
    private tokenStorageService: TokenStorageService,
    private userService: UserServiceService,
    private router: Router,
    private snackBar: MatSnackBar,
    private matDialog: MatDialog
  ) {
    this.currentUser = this.tokenStorageService.getUser();
    if (this.currentUser) {
      this.router.navigate(["/dashboard"]);
    }

    this.formLogin = this.formBuilder.group({
      username: ["", [Validators.required, Validators.minLength(3)]],
      password: ["", [Validators.required, Validators.minLength(6)]],
    });
  }

  get form() {
    return this.formLogin.controls;
  }

  ngOnInit(): void {
    if (this.tokenStorageService.getToken()) {
      this.isLoggedIn = true;
      this.roles = this.tokenStorageService.getUser().roles;
    }
  }

  onSubmit(): void {
    if (this.formLogin.invalid) {
      return;
    } else {
      this.blockUI.start();
      this.authService.login(this.formLogin.value).subscribe(
        (data) => {
          if (!data.data) {
            this.blockUI.stop();
            this.snackBar.open("Sai tên đăng nhập hoặc mật khẩu", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          } else {
            this.tokenStorageService.saveToken(data.data.token);
            this.tokenStorageService.saveUser(data);
            this.tokenStorageService.saveRole(data.data.roles);
            const intersection = data.data.roles.filter((element) =>
              ["Super admin", "Trung tâm Y tế", "Trạm Y tế"].includes(element)
            );
            if (intersection.length !== 0) {
              this.userService.getCurrentUser().subscribe((data) => {
                if (data) {
                  this.tokenStorageService.saveUser(data);
                  this.tokenStorageService.isLogin$.next(true);
                  this.router.navigate(["/dashboard"]);
                }
                this.blockUI.stop();
                this.snackBar.open("Đăng nhập thành công", "Ẩn", {
                  duration: 3000,
                  horizontalPosition: "right",
                  verticalPosition: "bottom",
                });
              });

            } else {
              this.openDialog();
              localStorage.clear();
              // this.snackBar.open('Không có quyền truy cập', 'Ẩn', {
              //   duration: 3000,
              //   horizontalPosition: 'right',
              //   verticalPosition: 'bottom',
              // });
              this.blockUI.stop();
            }
          }
        },
        (err) => {
          this.blockUI.stop();
          this.snackBar.open("Đăng nhập thất bại", "Ẩn", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "bottom",
          });
        }
      );
    }
  }
  openDialog() {
    this.matDialog.open(PermissionBlockComponent, {
      // data: {
      //   animal: 'panda',
      // },
    });
  }
  toggleShowPassword() {
    this.password = !this.password;
  }
  // reloadPage(): void {
  //   window.location.reload();
  // }
}
