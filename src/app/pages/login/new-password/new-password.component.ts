import { ActivatedRoute, Router } from "@angular/router";
import { Component, OnInit } from "@angular/core";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AuthServiceService } from "app/_service/JWT_service/auth-service.service";
import { MatSnackBar } from "@angular/material/snack-bar";
import { UserServiceService } from "app/_service/JWT_service/user-service.service";

@Component({
  selector: "app-new-password",
  templateUrl: "./new-password.component.html",
  styleUrls: ["./new-password.component.css"],
})
export class NewPasswordComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  formPassword: FormGroup;
  token: string;

  password: boolean;
  cfPassword: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthServiceService,
    private userService: UserServiceService,
    private router: Router,
    private snackBar: MatSnackBar,
    private route: ActivatedRoute
  ) {
    this.formPassword = this.formBuilder.group({
      password: ["", [Validators.required, Validators.minLength(6)]],
      cfPassword: ["", [Validators.required, Validators.minLength(6)]],
    });
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe((params) => {
      this.token = params["token"];
      // console.log(params['token']);
    });
  }
  toggleShowPassword() {
    this.password = !this.password;
  }

  toggleShowcfPassword() {
    this.cfPassword = !this.cfPassword;
  }
  onReset(): void {
    if (this.formPassword.invalid) {
      this.snackBar.open("Sai mật khẩu mới", "Ẩn", {
        duration: 3000,
        horizontalPosition: "right",
        verticalPosition: "bottom",
      });
      return;
    } else if (
      this.formPassword.get("password").value !==
      this.formPassword.get("cfPassword").value
    ) {
      this.snackBar.open("Mật khẩu mới không trùng", "Ẩn", {
        duration: 3000,
        horizontalPosition: "right",
        verticalPosition: "bottom",
      });
      return;
    } else {
      this.blockUI.start();
      this.userService
        .resetPassword(this.formPassword.value, this.token)
        .subscribe(
          (res) => {
            if (res) {
              this.blockUI.stop();
              this.router.navigate(["/login"]);
              this.snackBar.open("Đổi mật khẩu thành công", "Ẩn", {
                duration: 4000,
                horizontalPosition: "right",
                verticalPosition: "bottom",
              });
            }
          },
          (err) => {
            this.blockUI.stop();
            this.snackBar.open("Gửi email thất bại", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          }
        );
    }
  }
  onCancel() {
    this.router.navigate(["/login"]);
  }
}
