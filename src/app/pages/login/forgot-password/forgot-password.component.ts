import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatSnackBar } from "@angular/material/snack-bar";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthServiceService } from "app/_service/JWT_service/auth-service.service";
import { TokenStorageService } from "app/_service/JWT_service/token-storage.service";
import { UserServiceService } from "app/_service/JWT_service/user-service.service";
import { BlockUI, NgBlockUI } from "ng-block-ui";

@Component({
  selector: "app-forgot-password",
  templateUrl: "./forgot-password.component.html",
  styleUrls: ["./forgot-password.component.css"],
})
export class ForgotPasswordComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;

  errorMessage: string = "";
  roles: string[] = [];
  formPassword: FormGroup;
  isToken = false;
  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthServiceService,
    private tokenStorageService: TokenStorageService,
    private userService: UserServiceService,
    private router: Router,
    private snackBar: MatSnackBar,
    private route: ActivatedRoute
  ) {
    this.formPassword = this.formBuilder.group({
      mail: [
        "",
        [
          Validators.required,
          Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$"),
        ],
      ],
    });
    
  }

  ngOnInit(): void {
    this.route.queryParamMap.subscribe((params) => {
      console.log(params);
      // if(params.token){
      //   this.isToken = true;
      //   console.log("nopass");
      // }else{
      //   console.log("zz");
      // }
    });
  }

  onSubmit(): void {
    if (this.formPassword.invalid) {
      this.snackBar.open("Chưa nhập hoặc đã nhập sai email", "Ẩn", {
        duration: 3000,
        horizontalPosition: "right",
        verticalPosition: "bottom",
      });
      return;
    } else {
      this.blockUI.start();
      this.userService.forgotPassword(this.formPassword.value).subscribe(
        (res) => {
          if (res === 'Send mail success') {
            this.blockUI.stop();
            this.snackBar.open("Hệ thống đã gửi xác nhận qua email", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          } else{
            this.blockUI.stop();
            this.snackBar.open("Không gửi được email", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          }
        },
        (err) => {
          this.blockUI.stop();
          this.snackBar.open("Gửi email thất bại", "Ẩn", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "bottom",
          });
        }
      );
    }
  }
  
  onCancel() {
    this.router.navigate(["/login"]);
  }

  reloadPage(): void {
    window.location.reload();
  }
}
