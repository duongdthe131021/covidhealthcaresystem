import { FormBuilder, FormGroup } from "@angular/forms";
import { MatSnackBar } from "@angular/material/snack-bar";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { DailyReport, DailyReports } from "./../../../core/models/dailyReport";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { ReportService } from "app/_service/manager_service/report.service";
import * as moment from "moment";
import * as FileSaver from "file-saver";
import { FileManagementService } from "app/_service/manager_service/file-management.service";

@Component({
  selector: "app-report-by-user",
  templateUrl: "./report-by-user.component.html",
  styleUrls: ["./report-by-user.component.css"],
})
export class ReportByUserComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;

  reports: DailyReports;
  report: DailyReport;

  p: number = 1;
  id: string;
  name: string;

  currentYear: number;

  constructor(
    private route: ActivatedRoute,
    private fileService: FileManagementService,
    private service: ReportService,
    private snackBar: MatSnackBar
  ) {
    this.route.queryParams.subscribe((params) => {
      this.id = params.id;
      this.name = params.name;
      console.log(this.name);
    });
  }

  ngOnInit(): void {
    this.getReportByUserId();
  }

  getReportByUserId() {
    this.blockUI.start();
    this.service.getReportByUserId(this.id).subscribe(
      (data) => {
        this.blockUI.stop();
        this.reports = data;
        this.report = data[0];
        console.log(this.reports);
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }

  //export
  fileExtension = ".xlsx";
  onDownloadFile(event: any): void {
    if (!this.id) {
      this.snackBar.open("Không có báo cáo", "Ẩn", {
        duration: 3000,
        horizontalPosition: "right",
        verticalPosition: "bottom",
      });
      return;
    } else {
      this.blockUI.start();
      this.fileService.exportReportFile(this.id).subscribe(
        (data) => {
          const blob = new Blob([data], {
            type:
              "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
          });
          FileSaver.saveAs(
            blob,
            this.name + "-" + new Date().toLocaleDateString() + this.fileExtension
          );
          this.blockUI.stop();
        },
        (err) => {
          this.blockUI.stop();
          this.snackBar.open("Tải xuống lỗi", "Ẩn", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "bottom",
          });
        }
      );
    }
  }

  convertFullName(lastName, surName, firstName) {
    return lastName + " " + surName + " " + firstName;
  }
  convertGender(gender) {
    if (gender === "1") {
      return "Nam";
    } else if (gender === "2") {
      return "Nữ";
    } else {
      return "Không rõ";
    }
  }
  convertDate(date: string) {
    var t = moment(date, "HH:mm A");
    if (t.get("hour") <= 12) {
      return "Sáng";
    } else {
      return "Chiều";
    }
  }
  convert(arr) {
    let res = "";
    arr.forEach((item) => {
      res += `${item}\n`;
    });
    return res;
  }
  convertAge(yearOfBirth) {
    this.currentYear = new Date().getFullYear();
    var year = yearOfBirth.substr(0, 4);
    var age = this.currentYear - year;
    return age;
  }
  convertAddress(village, district, province) {
    return village + " - " + district + " - " + province;
  }
}
