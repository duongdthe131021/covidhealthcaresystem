import { MedicalStaff } from "./../../core/models/medicalStaff";
import { FormBuilder, FormGroup } from "@angular/forms";
import { MatSnackBar } from "@angular/material/snack-bar";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { Component, OnInit, Pipe, PipeTransform } from "@angular/core";
import { DailyReport, DailyReports } from "app/core/models/dailyReport";
import { ReportService } from "app/_service/manager_service/report.service";
import { TokenStorageService } from "app/_service/JWT_service/token-storage.service";
import { ListOfUser, User } from "app/core/models/user";
import { DatePipe } from "@angular/common";
import { DateAdapter } from "@angular/material/core";
import * as moment from "moment";
import * as FileSaver from "file-saver";
import { map } from "jquery";
import { AccountService } from "app/_service/manager_service/account.service";
import { AddressService } from "app/_service/manager_service/address.service";
import { ListOfProvince } from "app/core/models/address/province";
import { ListOfDistrict } from "d:/FPTU/Angular_Data/MAIN-PROJECT/gitlab_healthCareSystem/webapphealthcare/src/app/core/models/address/district";

@Component({
  selector: "app-report",
  templateUrl: "./report.component.html",
  styleUrls: ["./report.component.css"],
})
@Pipe({ name: "split" })
export class ReportComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  dailyReports: DailyReports;
  dailyReport: DailyReport;
  dailyReportOrigin: DailyReports;

  fullName: string;
  p: number = 1;
  currentUser: User;
  isSuperAdmin = false;
  isAdmin = false;
  isMod = false;
  villageId: number;

  currentDate = new Date();

  defaultVillageId: number;
  listOfProvince: ListOfProvince;
  listOfDistrict: ListOfDistrict;
  listOfMedicalStaff: ListOfUser;
  defaultDistrictId: number;

  constructor(
    private reportService: ReportService,
    private accountService: AccountService,
    private addressService: AddressService,
    private snackBar: MatSnackBar,
    private tokenStorageService: TokenStorageService,
    private dateAdapter: DateAdapter<Date>
  ) {
    this.dateAdapter.setLocale("en-GB"); //dd/MM/yyyy
  }
  ngOnInit(): void {
    this.currentUser = this.tokenStorageService.getUser();
    if (this.currentUser.roleName === "Super admin") {
      this.isSuperAdmin = true;
      this.getProvince();
    } else if (this.currentUser.roleName === "Trung tâm Y tế") {
      this.isAdmin = true;
      this.getMedicalStaffByAdmin(this.currentUser.districtId);
    } else {
      this.isMod = true;
      this.getReportByVillageId(this.currentUser.villageId);
    }
  }

  //superAdmin
  getProvince() {
    this.blockUI.start();
    this.addressService.getProvince().subscribe(
      (data) => {
        this.blockUI.stop();
        this.listOfProvince = data.data;
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }
  onChangeProvinceId(value) {
    this.listOfDistrict = null;
    this.listOfMedicalStaff = null;
    this.dailyReport = null;
    this.getDistrict(value);
  }
  getDistrict(provinceId) {
    this.blockUI.start();
    this.addressService.getDistrict(provinceId).subscribe(
      (data) => {
        this.blockUI.stop();
        this.listOfDistrict = data.data;
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }
  onChangeDistrictId(value) {
    this.listOfMedicalStaff = null;
    this.dailyReport = null;
    this.defaultDistrictId = value;
    this.getMedicalStaffByAdmin(value);
  }

  //isAdmin

  getMedicalStaffByAdmin(districtId) {
    this.blockUI.start();
    this.accountService.getMedicalStaff(districtId).subscribe((data) => {
      this.blockUI.stop();
      if (data.data.length != 0) {
        this.listOfMedicalStaff = data.data;
      } else {
        this.blockUI.stop();
        this.snackBar.open("Chưa có trạm y tế nào", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    });
  }
  onChangeMedicalStaff(value) {
    this.defaultVillageId = value;
    this.getReportByVillageId(this.defaultVillageId);
  }

  getDateValue(date) {
    this.p = 1;
    this.currentDate = date;
    const formatDate = moment(date).format("YYYY-MM-DD");
    if (formatDate) {
      this.dailyReports = this.dailyReportOrigin?.filter(
        (item) => item.date === formatDate
      );
    } else {
      this.dailyReports = this.dailyReportOrigin;
    }
  }
  getReportByVillageId(villageId) {
    this.blockUI.start();
    this.dailyReports = [];
    this.reportService.getReportByVillageId(villageId).subscribe(
      (data) => {
        if (data) {
          this.blockUI.stop();
          this.dailyReports = data;
          this.dailyReportOrigin = data;
          const formatDate = moment(this.currentDate).format("YYYY-MM-DD");
          this.dailyReports = this.dailyReportOrigin.filter(
            (item) => item.date === formatDate
          );
        } else {
          this.blockUI.stop();
          this.snackBar.open("Tải trang lỗi", "Ẩn", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "bottom",
          });
        }
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }

  getFullName(lastName, surName, firstName) {
    return lastName + " " + surName + " " + firstName;
  }

  // getCurrency(data) {
  //   return data + 'VND';
  // }
  transform(value: string, [separator]): string {
    let splits = value.split(separator);
    if (splits.length > 1) {
      return splits.pop();
    } else {
      return "";
    }
  }

  convert(arr) {
    let res = "";
    arr.forEach((item) => {
      res += `${item}\n`;
    });
    return res;
  }
}
