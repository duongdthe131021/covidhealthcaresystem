import { MatSnackBar } from "@angular/material/snack-bar";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { DailyReport, DailyReports } from "./../../../core/models/dailyReport";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Symptom } from "app/core/models/symptom";
import { ReportService } from "app/_service/manager_service/report.service";
import { User } from "app/core/models/user";

@Component({
  selector: "app-report-detail",
  templateUrl: "./report-detail.component.html",
  styleUrls: ["./report-detail.component.css"],
})
export class ReportDetailComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;

  reports: DailyReports;
  report: DailyReport;
  user = {
    name: '',
  }
  p: number = 1;
  id: string;
  name: string;

  symptom: Symptom[];
  currentYear: number;

  constructor(
    private route: ActivatedRoute,
    private service: ReportService,
    private snackBar: MatSnackBar
  ) {
    this.route.queryParams.subscribe((params) => {
      this.id = params.id;
      this.name = params.name;
      console.log(this.name);
    });
    
  }

  ngOnInit(): void {
    this.getReportByUserId();
  }

  getReportByUserId() {
    this.blockUI.start();
    this.service.getReportByUserId(this.id).subscribe(
      (data) => {
        this.blockUI.stop();
        this.reports = data;
        this.report = data[0];
        console.log(this.reports);
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }
  getFullName(lastName, surName, firstName) {
    return lastName + " " + surName + " " + firstName;
  }
  getGender(gender) {
    if (gender === "1") {
      return "Nam";
    } else if (gender === "2") {
      return "Nữ";
    } else {
      return "Không rõ";
    }
  }
  convert(arr) {
    let res = "";
    arr.forEach((item) => {
      res += `${item}\n`;
    });
    return res;
  }
  getAge(yearOfBirth) {
    this.currentYear = new Date().getFullYear();
    var year = yearOfBirth.substr(0, 4);
    var age = this.currentYear - year;
    return age;
  }
  getAddress(village, district, province) {
    return village + " - " + district + " - " + province;
  }
}
