import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatSnackBar } from "@angular/material/snack-bar";
import { ActivatedRoute, Router } from "@angular/router";
import { Exercise, ExerciseStep } from "app/core/models/exercise";
import { ExerciseService } from "app/_service/manager_service/exercise.service";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { MatDialog } from "@angular/material/dialog";
import { TokenStorageService } from "app/_service/JWT_service/token-storage.service";
import { User } from "app/core/models/user";
import { DeleteComponent } from "app/pages/dialog/delete/delete.component";
import { Observable, Subscriber } from "rxjs";
import { ExerciseStepEditComponent } from "../exercise-step-edit/exercise-step-edit.component";

@Component({
  selector: "app-exercise-detail",
  templateUrl: "./exercise-detail.component.html",
  styleUrls: ["./exercise-detail.component.css"],
})
export class ExerciseDetailComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  formCreate: FormGroup;

  exercise: Exercise;
  exerciseStep: ExerciseStep[];
  id: string;
  defaultImg = "assets/images/default-exercise.jpg";
  // defaultImg = "";
  baseImg =
    "https://healthcaresystemstorage.s3.us-east-2.amazonaws.com/images/";
  currentUser: User;
  isSuperAdmin = false;
  isAdmin = false;
  isEmpty = false;
  isBase64 = false;
  exerciseId: number;

  constructor(
    private tokenStorageService: TokenStorageService,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private exerciseService: ExerciseService,
    private router: Router,
    private matDialog: MatDialog
  ) {
    this.route.queryParams.subscribe((params) => {
      this.id = params.id;
    });
  }

  ngOnInit(): void {
    this.currentUser = this.tokenStorageService.getUser();
    if (this.currentUser.roleName === "Super admin") {
      this.isSuperAdmin = true;
    }
    this.getExerciseById();
    this.getExerciseDetailById();
    this.initFormCreate();
  }
  initFormCreate() {
    this.formCreate = this.formBuilder.group({
      exerciseId: [this.id, Validators.required],
      description: [null, Validators.required],
      detail: [null, Validators.required],
      image_description: [null],
    });
  }
  getExerciseById() {
    this.blockUI.start();
    this.exerciseService.getExerciseById(this.id).subscribe(
      (data) => {
        this.blockUI.stop();
        this.exercise = data.data;
        this.exerciseId = this.exercise.id;
        this.formCreate.patchValue({
          exerciseId: this.exerciseId,
        });
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }
  getExerciseDetailById() {
    this.blockUI.start();
    this.exerciseService.getExerciseDetailById(this.id).subscribe(
      (data) => {
        this.blockUI.stop();
        if (data.data.length != 0) {
          this.exerciseStep = data.data;
        } else {
          this.isEmpty = true;
          this.snackBar.open("Chưa có hướng dẫn tập", "Ẩn", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "bottom",
          });
        }
      },
      (err) => {
        this.blockUI.stop();
        this.isEmpty = true;
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }
  onCreateStep() {
    console.log(this.formCreate.value);
    if (this.formCreate.invalid) {
      this.snackBar.open("Chưa đủ thông tin bài tập", "Ẩn", {
        duration: 3000,
        horizontalPosition: "right",
        verticalPosition: "bottom",
      });
      return;
    } else {
      this.blockUI.start();
      this.exerciseService.exerciseStepCreate(this.formCreate.value).subscribe(
        (response) => {
          this.blockUI.stop();
          if (response) {
            this.getExerciseDetailById();
            this.formCreate.reset();
            this.formCreate.patchValue({
              exerciseId: this.exerciseId,
            });
            this.defaultImg = "assets/images/default-exercise.jpg";
            this.snackBar.open("Tạo mới thành công", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          } else {
            this.blockUI.stop();
            this.snackBar.open("Tạo mới thất bại", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          }
        },
        (err) => {
          this.blockUI.stop();
          this.snackBar.open("Lỗi hệ thống", "Ẩn", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "bottom",
          });
        }
      );
    }
  }
  onDelete(id) {
    const dialogRef = this.matDialog.open(DeleteComponent, {
      data: {
        message: "Xóa dữ liệu?",
        type: "exercise-detail",
        id: id,
        buttonText: {
          ok: "Xóa",
          cancel: "Hủy",
        },
      },
    });
    dialogRef.afterClosed().subscribe((data) => {
      this.exerciseStep = [];
      this.getExerciseDetailById();
    });
  }

  onSelectFile(event) {
    const file = event.target.files[0];
    this.convertToBase64(file);
  }
  convertToBase64(file: File) {
    const observable = new Observable((subscriber: Subscriber<any>) => {
      this.readFile(file, subscriber);
    });
    observable.subscribe(
      (data) => {
        this.defaultImg = data;
        this.formCreate.patchValue({
          image_description: data,
        });
      },
      (error) => {
        alert(error);
      }
    );
  }
  onEditBtn(id: string){
    this.router.navigate(['/exercise-edit'], { queryParams: { id: id } })
  }
  readFile(file: File, subscriber: Subscriber<any>) {
    const fileReader = new FileReader();
    fileReader.readAsDataURL(file);

    fileReader.onload = () => {
      subscriber.next(fileReader.result);
      subscriber.complete();
    };
    fileReader.onerror = (error) => {
      subscriber.error(error);
      subscriber.complete();
    };
  }
  convertImage(value) {
    return this.baseImg + value;
  }
  onStepEdit(exerciseStep) {
    const dialogRef = this.matDialog.open(ExerciseStepEditComponent, {
      data: {
        exerciseId: this.exerciseId,
        exerciseStepId: exerciseStep.exerciseDetailId,
        description: exerciseStep.description,
        detail: exerciseStep.detail,
        image_description: exerciseStep.imageDescription,
      },
      width: "700px",
      panelClass: "epsSelectorPanel",
    });
    dialogRef.afterClosed().subscribe((data) => {
      this.getExerciseDetailById();
    });
    
  }
}
