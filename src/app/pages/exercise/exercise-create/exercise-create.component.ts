import { Router } from "@angular/router";
import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatSnackBar } from "@angular/material/snack-bar";
import { UserServiceService } from "app/_service/JWT_service/user-service.service";
import { ExerciseService } from "app/_service/manager_service/exercise.service";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { Observable, Subscriber } from "rxjs";
import { User } from "app/core/models/user";
import { TokenStorageService } from "app/_service/JWT_service/token-storage.service";

@Component({
  selector: "app-exercise-create",
  templateUrl: "./exercise-create.component.html",
  styleUrls: ["./exercise-create.component.css"],
})
export class ExerciseCreateComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  formCreate: FormGroup;

  defaultImg = "/assets/images/default-exercise.jpg";
  baseImg =
    "https://healthcaresystemstorage.s3.us-east-2.amazonaws.com/images/";
  currentUser: User;

  constructor(
    private tokenStorageService: TokenStorageService,
    private router: Router,
    private formBuilder: FormBuilder,
    private exerciseService: ExerciseService,
    private snackBar: MatSnackBar
  ) {
    this.currentUser = this.tokenStorageService.getUser();
    if (
      this.currentUser.roleName === "Trung tâm Y tế" ||
      this.currentUser.roleName === "Trạm Y tế"
    ) {
      this.router.navigate(["/dashboard"]);
    }
  }

  ngOnInit(): void {
    this.initForm();
  }
  initForm() {
    this.formCreate = this.formBuilder.group({
      name: ["", [Validators.required, Validators.minLength(3)]],
      filePath: [null, Validators.required],
      description: ["", Validators.required],

    });
  }
  onSubmit() {
    if (this.formCreate.invalid) {
      this.snackBar.open("Chưa đủ thông tin bài tập", "Ẩn", {
        duration: 3000,
        horizontalPosition: "right",
        verticalPosition: "bottom",
      });
      return;
    } else {
      this.blockUI.start();
      this.exerciseService.exerciseCreate(this.formCreate.value).subscribe(
        (response) => {
          this.blockUI.stop();
          if (response.code === "CREATE_EXERCISE_SUCCESS") {
            this.formCreate.reset();
            this.defaultImg = "/assets/images/default-exercise.jpg";
            this.snackBar.open("Tạo mới thành công", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          } else {
            this.blockUI.stop();
            this.snackBar.open("Tạo mới thất bại", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          }
        },
        (err) => {
          this.blockUI.stop();
          this.snackBar.open("Lỗi hệ thống", "Ẩn", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "bottom",
          });
        }
      );
    }
  }
  onSelectFile(event) {
    const file = event.target.files[0];
    this.convertToBase64(file);
  }
  convertToBase64(file: File) {
    const observable = new Observable((subscriber: Subscriber<any>) => {
      this.readFile(file, subscriber);
    });
    observable.subscribe(
      (data) => {
        this.defaultImg = data;
        this.formCreate.patchValue({
          filePath: data,
        });
      },
      (error) => {
        alert(error);
      }
    );
  }

  readFile(file: File, subscriber: Subscriber<any>) {
    const fileReader = new FileReader();
    fileReader.readAsDataURL(file);

    fileReader.onload = () => {
      subscriber.next(fileReader.result);
      subscriber.complete();
    };
    fileReader.onerror = (error) => {
      subscriber.error(error);
      subscriber.complete();
    };
  }
  onCancel() {
    this.formCreate.reset();
  }
}
