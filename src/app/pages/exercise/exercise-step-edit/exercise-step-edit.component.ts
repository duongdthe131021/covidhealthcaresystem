import { ActivatedRoute, Router } from "@angular/router";
import { Component, Inject, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatSnackBar } from "@angular/material/snack-bar";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { ExerciseService } from "app/_service/manager_service/exercise.service";
import { Exercise, ExerciseStep } from "app/core/models/exercise";
import { User } from "app/core/models/user";
import { TokenStorageService } from "app/_service/JWT_service/token-storage.service";
import { Observable, Subscriber } from "rxjs";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
@Component({
  selector: "app-exercise-step-edit",
  templateUrl: "./exercise-step-edit.component.html",
  styleUrls: ["./exercise-step-edit.component.css"],
})
export class ExerciseStepEditComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  formUpdate: FormGroup;

  currentUser: User;
  id: string;
  exercise: Exercise;
  exerciseStep: ExerciseStep[];
  isEmpty = false;
  defaultImg = "/assets/images/default-exercise.jpg";
  baseImg =
    "https://healthcaresystemstorage.s3.us-east-2.amazonaws.com/images/";

  constructor(
    @Inject(MAT_DIALOG_DATA) private data: any,
    private tokenStorageService: TokenStorageService,
    private dialogRef: MatDialogRef<ExerciseStepEditComponent>,
    private router: Router,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private exerciseService: ExerciseService
  ) {
    this.route.queryParams.subscribe((params) => {
      this.id = params.id;
    });
    this.currentUser = this.tokenStorageService.getUser();
    if (
      this.currentUser.roleName === "Trung tâm Y tế" ||
      this.currentUser.roleName === "Trạm Y tế"
    ) {
      this.router.navigate(["/dashboard"]);
    }
  }

  ngOnInit(): void {
    if (this.data.image_description) {
      this.defaultImg = this.baseImg + this.data.image_description;
    }
    this.formUpdate = this.formBuilder.group({
      exerciseId: [this.id, Validators.required],
      description: [this.data.description, Validators.required],
      detail: [this.data.detail, Validators.required],
      image_description: [this.data.image_description],
    });
  }

  onEditStep() {
    if (this.formUpdate.invalid) {
      this.snackBar.open("Chưa đủ thông tin bài tập", "Ẩn", {
        duration: 3000,
        horizontalPosition: "right",
        verticalPosition: "bottom",
      });
      return;
    } else {
      this.blockUI.start();
      this.exerciseService
        .exerciseStepUpdate(this.formUpdate.value, this.data.exerciseStepId)
        .subscribe(
          (response) => {
            this.blockUI.stop();
            this.dialogRef.close();
            this.snackBar.open("Chỉnh sửa thành công", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          },
          (err) => {
            this.blockUI.stop();
            this.snackBar.open("Chỉnh sửa thất bại", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          }
        );
    }
  }
  onSelectFile(event) {
    const file = event.target.files[0];
    this.convertToBase64(file);
  }
  convertToBase64(file: File) {
    const observable = new Observable((subscriber: Subscriber<any>) => {
      this.readFile(file, subscriber);
    });
    observable.subscribe(
      (data) => {
        this.defaultImg = data;
        this.formUpdate.patchValue({
          image_description: data,
        });
      },
      (error) => {
        alert(error);
      }
    );
  }
  readFile(file: File, subscriber: Subscriber<any>) {
    const fileReader = new FileReader();
    fileReader.readAsDataURL(file);

    fileReader.onload = () => {
      subscriber.next(fileReader.result);
      subscriber.complete();
    };
    fileReader.onerror = (error) => {
      subscriber.error(error);
      subscriber.complete();
    };
  }
  convertImage(value) {
    return this.baseImg + value;
  }
  // getImageSrc(exercise) {
  //   return exercise?.imageBase64
  //     ? exercise.imageBase64
  //     : this.convertImage(exercise.imageDescription)
  //     ? this.convertImage(exercise.imageDescription)
  //     : "";
  // }
  onCancel() {
    this.formUpdate.patchValue({
      exerciseId: this.id,
      description: this.data.description,
      detail: this.data.detail,
      image_description: this.data.image_description,
    });
    if (this.data.image_description) {
      this.defaultImg = this.baseImg + this.data.image_description;
    } else {
      this.defaultImg = "/assets/images/default-exercise.jpg";
    }
  }
}
