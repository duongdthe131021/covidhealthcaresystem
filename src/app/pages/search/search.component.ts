import { ListOfReport } from "./../../core/models/dailyReport";
import { ListOfUser } from "./../../core/models/user";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { ActivatedRoute, Router } from "@angular/router";
import { Component, OnInit, Input } from "@angular/core";

import { SearchService } from "app/_service/manager_service/search.service";
import { User, UserResponse } from "app/core/models/user";
import { MatSnackBar } from "@angular/material/snack-bar";
import { ListOfResult } from "app/core/models/test-result";

@Component({
  selector: "app-search",
  templateUrl: "./search.component.html",
  styleUrls: ["./search.component.css"],
})
export class SearchComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;

  userName: string;
  user: User;
  listOfUser: ListOfUser;
  listOfReport: ListOfReport;
  listOfResult: ListOfResult;

  p: number = 1;

  defaultAvatar = "assets/images/default-avatar.jpg";
  baseImg =
    "https://healthcaresystemstorage.s3.us-east-2.amazonaws.com/images/";
  

  constructor(
    private service: SearchService,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private router: Router,
    private searchService: SearchService
  ) {
    this.route.queryParams.subscribe((params) => {
      this.userName = params.name;
      this.searchUserByUserName(this.userName);
    });
    this.searchService.searchInput$.subscribe((data) => {
      console.log(data);
      this.searchByUrl(data.url, data.name);
    });
  }

  searchByUrl(url, name) {
    switch (url) {
      // case "/report":
      //   this.searchReportByUserName(name)
      //   break;
      default:
        console.log("call api user");
    }
  }

  ngOnInit(): void {}
  searchUserByUserName(name) {
    this.blockUI.start();
    this.service.searchUserByUserName(name, 252).subscribe(
      (data) => {
        this.blockUI.stop();
        this.listOfUser = data;
        // for (var i = 0; i < this.listOfUser.length; i++) {
        //   if (this.listOfUser[i].avatar != null) {
        //     this.avatar = this.baseImg + this.listOfUser[i].avatar;
        //   }
        // }
      },
      (error) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }
  searchReportByUserName(name) {
    this.blockUI.start();
    this.service.searchReportByUserName(name, 252).subscribe(
      (data) => {
        this.blockUI.stop();
        this.listOfReport = data;
      },
      (error) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }
  searchResultByUserName(name) {
    this.blockUI.start();
    this.service.searchResultByUserName(name, 252).subscribe(
      (data) => {
        this.blockUI.stop();
        this.listOfResult = data;
      },
      (error) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }
  getFullName(lastName, surName, firstName) {
    return lastName + " " + surName + " " + firstName;
  }
  getAddress(village, district, province) {
    return village + " - " + district + " - " + province;
  }
}
