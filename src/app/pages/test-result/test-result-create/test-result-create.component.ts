import { AddressService } from "app/_service/manager_service/address.service";
import { TokenStorageService } from "./../../../_service/JWT_service/token-storage.service";
import { AccountService } from "./../../../_service/manager_service/account.service";
import { FileManagementService } from "./../../../_service/manager_service/file-management.service";
import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { MatSnackBar } from "@angular/material/snack-bar";
import { TestResultService } from "app/_service/manager_service/test-result.service";
import { ListOfUser, User } from "app/core/models/user";
import { ListOfProvince } from "app/core/models/address/province";
import { ListOfDistrict } from "app/core/models/address/district";
import * as moment from "moment";

@Component({
  selector: "app-test-result-create",
  templateUrl: "./test-result-create.component.html",
  styleUrls: ["./test-result-create.component.css"],
})
export class TestResultCreateComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  @ViewChild("fileUpload") fileUpload;
  formCreate: FormGroup;

  selectedFiles?: FileList;
  selectedFileName = "";
  currentFile?: File;
  sampleFile =
    "https://healthcaresystemstorage.s3.us-east-2.amazonaws.com/ExampleExcel/Mau_ket_qua_xet_nghiem_3_1.xlsx";

  currentUser: User;
  listOfProvince: ListOfProvince;
  listOfDistrict: ListOfDistrict;
  listOfMedicalStaff: ListOfUser;
  ListOfUser: ListOfUser;
  defaultProvinceId: number;
  defaultDistrictId: number;
  defaultVillageId: number;
  isSuperAdmin = false;
  isAdmin = false;
  isMod = false;
  userId: number;
  minDate = moment().format("YYYY-MM-DD");
  maxDate = moment().format("YYYY-MM-DD");
  selectedUser: number;

  constructor(
    private formBuilder: FormBuilder,
    private fileService: FileManagementService,
    private accountService: AccountService,
    private addressService: AddressService,
    private snackBar: MatSnackBar,
    private testResultService: TestResultService,
    private tokenStorageService: TokenStorageService
  ) {
    this.formCreate = this.formBuilder.group({
      resultCode: ["", Validators.required],
      userId: [this.userId, Validators.required],
      unitSend: ["", Validators.required],
      numberTest: [],
      startDate: [null],
      collectDate: [, Validators.required],
      receiveDate: [, Validators.required],
      testDate: [, Validators.required],
      sampleType: ["Dịch Họng", Validators.required],
      status: [1],
      testResult: [1, Validators.required],
      comment: [],
    });
  }

  ngOnInit(): void {
    this.currentUser = this.tokenStorageService.getUser();
    if (this.currentUser.roleName === "Super admin") {
      this.isSuperAdmin = true;
      this.getProvince();
    } else if (this.currentUser.roleName === "Trung tâm Y tế") {
      this.isAdmin = true;
      this.getMedicalStaff(this.currentUser.districtId);
    } else {
      this.isMod = true;
      this.getPatientByVillage(this.currentUser.villageId);
    }
  }

  //superAdmin
  getProvince() {
    this.blockUI.start();
    this.addressService.getProvince().subscribe(
      (data) => {
        this.blockUI.stop();
        this.listOfProvince = data.data;
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }
  onChangeProvinceId(value) {
    if (value) {
      this.defaultDistrictId = null;
      this.defaultProvinceId = value;
      this.ListOfUser = null;
      this.getDistrict(value);
    } else {
      return;
    }
  }
  getDistrict(provinceId) {
    this.blockUI.start();
    this.addressService.getDistrict(provinceId).subscribe(
      (data) => {
        this.blockUI.stop();
        this.listOfDistrict = data.data;
      },
      (err) => {
        this.blockUI.stop();
        this.defaultDistrictId = null;
        this.defaultVillageId = null;
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }
  onChangeDistrictId(value) {
    if (value) {
      this.defaultDistrictId = value;
      this.defaultVillageId = null;
      this.getMedicalStaff(value);
      this.ListOfUser = null;
    } else {
      return;
    }
  }

  //sample file download

  getMedicalStaff(districtId) {
    this.blockUI.start();
    this.accountService.getMedicalStaff(districtId).subscribe(
      (data) => {
        this.blockUI.stop();
        if (data.data.length != 0) {
          this.listOfMedicalStaff = data.data;
          //on default value
          // this.defaultVillageId = this.listOfMedicalStaff[0]?.village.villageId;
          // this.getPatientByVillage(this.defaultVillageId);
        } else {
          this.blockUI.stop();
          this.snackBar.open("Chưa có trạm y tế nào", "Ẩn", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "bottom",
          });
        }
      }
      // (err) => {
      //   this.blockUI.stop();
      //   this.snackBar.open("Lỗi hệ thống", "Ẩn", {
      //     duration: 3000,
      //     horizontalPosition: "right",
      //     verticalPosition: "bottom",
      //   });
      // }
    );
  }
  onChangeVillage(villageValue) {
    this.defaultVillageId = villageValue;
    this.getPatientByVillage(this.defaultVillageId);
  }
  onChangeUser(userId) {
    this.userId = userId;
    this.formCreate.patchValue({
      userId: this.userId,
    });
  }

  getPatientByVillage(villageId) {
    this.blockUI.start();
    this.accountService.getPatientByVillageId(villageId).subscribe(
      (data) => {
        this.blockUI.stop();
        if (data) {
          this.ListOfUser = data.data;
          // this.userId = this.ListOfUser[0].userId;
          // this.formCreate.patchValue({
          //   userId: this.userId,
          // });
        } else {
          this.blockUI.stop();
          this.snackBar.open("Không có dữ liệu", "Ẩn", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "bottom",
          });
        }
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }
  onSampleFileDownload() {
    const link = document.createElement("a");
    link.setAttribute("target", "_blank");
    link.setAttribute(
      "href",
      "https://healthcaresystemstorage.s3.us-east-2.amazonaws.com/ExampleExcel/Mau_ket_qua_xet_nghiem_3_1.xlsx"
    );
    link.setAttribute("download", `products.csv`);
    document.body.appendChild(link);
    link.click();
    link.remove();
  }
  //file Upload
  onSelectFile(event: any): void {
    this.blockUI.start();
    this.selectedFiles = event.target.files;
    this.selectedFileName = event.target.files[0].name;
    if (this.selectedFiles) {
      const file: File | null = this.selectedFiles.item(0);
      if (file) {
        this.currentFile = file;
        this.fileService.importTestResultFile(this.currentFile).subscribe(
          (response) => {
            this.blockUI.stop();
            if (response == "Import file successfully") {
              this.snackBar.open("Tải lên thành công", "Ẩn", {
                duration: 3000,
                horizontalPosition: "right",
                verticalPosition: "bottom",
              });
            } else {
              this.snackBar.open("Tải lên thất bại", "Ẩn", {
                duration: 3000,
                horizontalPosition: "right",
                verticalPosition: "bottom",
              });
            }
          },
          (err) => {
            this.snackBar.open("Lỗi hệ thống", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          }
        );
        this.fileUpload.nativeElement.value = "";
      }
    }
  }
  onSubmit() {
    console.log(this.formCreate);
    if (this.formCreate.invalid || !this.userId) {
      this.snackBar.open("Chưa đủ hoặc sai thông tin", "Ẩn", {
        duration: 3000,
        horizontalPosition: "right",
        verticalPosition: "bottom",
      });
      return;
    } else {
      this.blockUI.start();
      this.testResultService
        .testResultCreate(this.userId, this.formCreate.value)
        .subscribe(
          (response) => {
            this.blockUI.stop();
            if (response) {
              this.formCreate.reset();
              this.formCreate.patchValue({userId: this.userId});
              this.snackBar.open("Tạo mới thành công", "Ẩn", {
                duration: 3000,
                horizontalPosition: "right",
                verticalPosition: "bottom",
              });
            }
          },
          (err) => {
            this.blockUI.stop();
            if (err.error.error === "USER_NOT_FOUND") {
              this.snackBar.open("Không tìm thấy người dùng", "Ẩn", {
                duration: 3000,
                horizontalPosition: "right",
                verticalPosition: "bottom",
              });
            } else if (err.error.error === "RESULT_CODE_ISEXISTED") {
              this.snackBar.open("Mã xét nghiệm đã tồn tại", "Ẩn", {
                duration: 3000,
                horizontalPosition: "right",
                verticalPosition: "bottom",
              });
            } else {
              this.snackBar.open("Tạo mới thất bại", "Ẩn", {
                duration: 3000,
                horizontalPosition: "right",
                verticalPosition: "bottom",
              });
            }
          }
        );
    }
  }
  onCancel() {
    this.formCreate.reset();
    this.formCreate.patchValue({userId: this.userId});
  }
  getFullName(lastName, surName, firstName) {
    return lastName + " " + surName + " " + firstName;
  }
}
