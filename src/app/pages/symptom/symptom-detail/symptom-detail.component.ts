import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatSnackBar } from "@angular/material/snack-bar";
import { ActivatedRoute, Router } from "@angular/router";
import { Symptom } from "app/core/models/symptom";
import { User } from "app/core/models/user";
import { TokenStorageService } from "app/_service/JWT_service/token-storage.service";
import { ReportService } from "app/_service/manager_service/report.service";
import { SymptomService } from "app/_service/manager_service/symptom.service";
import { BlockUI, NgBlockUI } from "ng-block-ui";

@Component({
  selector: "app-symptom-detail",
  templateUrl: "./symptom-detail.component.html",
  styleUrls: ["./symptom-detail.component.css"],
})
export class SymptomDetailComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;

  symptom: Symptom;
  id: string;
  formUpdate: FormGroup;
  currentUser: User;

  constructor(
    private tokenStorageService: TokenStorageService,
    private router: Router,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private symptomService: SymptomService
  ) {
    this.route.queryParams.subscribe((params) => {
      this.id = params.id;
    });
    this.currentUser = this.tokenStorageService.getUser();
    if (
      this.currentUser.roleName === "Trung tâm Y tế" ||
      this.currentUser.roleName === "Trạm Y tế"
    ) {
      this.router.navigate(["/dashboard"]);
    }
  }

  ngOnInit(): void {
    this.getSymptomById();
    this.initForm();
  }
  initForm(symptom?) {
    this.formUpdate = this.formBuilder.group({
      symptomId: this.id,
      name: [symptom?.name, Validators.required],
      description: [symptom?.description],
      isActive: 1,
    });
  }
  getSymptomById() {
    this.blockUI.start();
    this.symptomService.getSymptomById(this.id).subscribe(
      (data) => {
        this.symptom = data.data;
        this.initForm(this.symptom);
        this.blockUI.stop();
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }
  onSubmit() {
    if (this.formUpdate.invalid) {
      return;
    } else {
      this.blockUI.start();
      this.symptomService.symptomUpdate(this.formUpdate.value).subscribe(
        (res) => {
          if (res) {
            this.blockUI.stop();
            if (res.code == "SUCCESS") {
              this.router.navigate(["/symptom"]);
              this.snackBar.open("Cập nhật thành công", "Ẩn", {
                duration: 3000,
                horizontalPosition: "right",
                verticalPosition: "bottom",
              });
            } else if (res.code == "SYMPTOM_NAME_EXISTED") {
              this.snackBar.open("Đã trùng tên", "Ẩn", {
                duration: 3000,
                horizontalPosition: "right",
                verticalPosition: "bottom",
              });
            }
          } else {
            this.blockUI.stop();
            this.snackBar.open("Cập nhật thất bại", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          }
        },
        (error) => {
          this.blockUI.stop();
          this.snackBar.open("Lỗi hệ thống", "Ẩn", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "bottom",
          });
        }
      );
    }
  }
  onCancel() {
    this.router.navigate(["/symptom"]);
  }
}
