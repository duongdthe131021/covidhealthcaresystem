import { MatSnackBar } from "@angular/material/snack-bar";
import { Component, OnInit } from "@angular/core";
import { Symptom } from "app/core/models/symptom";
import { ReportService } from "app/_service/manager_service/report.service";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { MatDialog } from "@angular/material/dialog";
import { DeleteComponent } from "../dialog/delete/delete.component";
import { SymptomService } from "app/_service/manager_service/symptom.service";
import { User } from "app/core/models/user";
import { TokenStorageService } from "app/_service/JWT_service/token-storage.service";

@Component({
  selector: "app-symptom",
  templateUrl: "./symptom.component.html",
  styleUrls: ["./symptom.component.css"],
})
export class SymptomComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;

  symptoms: Symptom[];
  p: number = 1;

  currentUser: User;
  isSuperAdmin = false;
  isAdmin = false;

  constructor(
    private tokenStorageService: TokenStorageService,
    private symptomService: SymptomService,
    private snackBar: MatSnackBar,
    private matDialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.currentUser = this.tokenStorageService.getUser();
    if (this.currentUser.roleName === "Super admin") {
      this.isSuperAdmin = true;
    } else if(this.currentUser.roleName === "Trung tâm Y tế"){
      this.isAdmin = true;
    }
    this.getSymptomData();
  }

  getSymptomData() {
    this.blockUI.start();
    this.symptomService.getSymptom().subscribe(
      (data) => {
        this.blockUI.stop();
        this.symptoms = data.data;
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Tải trang lỗi", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }
  getStatus(status) {
    if (status === 0) {
      return "Ngừng hoạt động";
    } else if (status === 1) {
      return "Hoạt động";
    } else {
      return "Không rõ";
    }
  }
  openDialog(id) {
    const dialogRef = this.matDialog.open(DeleteComponent, {
      data: {
        message: "Xóa dữ liệu?",
        type: "symptom",
        id: id,
        buttonText: {
          ok: "Xóa",
          cancel: "Hủy",
        },
      },
    });
    dialogRef.afterClosed().subscribe((data) => {
      this.getSymptomData();
    });
  }
}
