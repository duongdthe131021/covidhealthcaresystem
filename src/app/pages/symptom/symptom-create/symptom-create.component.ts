import { BlockUI } from "ng-block-ui";
import { NgBlockUI } from "ng-block-ui";
import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatSnackBar } from "@angular/material/snack-bar";
import { SymptomService } from "app/_service/manager_service/symptom.service";
import { User } from "app/core/models/user";
import { TokenStorageService } from "app/_service/JWT_service/token-storage.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-symptom-create",
  templateUrl: "./symptom-create.component.html",
  styleUrls: ["./symptom-create.component.css"],
})
export class SymptomCreateComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  formCreate: FormGroup;

  currentUser: User;
  isSuperAdmin = false;
  isAdmin = false;
  
  constructor(
    private tokenStorageService: TokenStorageService,
    private router: Router,
    private formBuilder: FormBuilder,
    private symptomService: SymptomService,
    private snackBar: MatSnackBar
  ) {
    this.currentUser = this.tokenStorageService.getUser();
    if (
      this.currentUser.roleName === "Trung tâm Y tế" ||
      this.currentUser.roleName === "Trạm Y tế"
    ) {
      this.router.navigate(["/dashboard"]);
    }
  }

  ngOnInit(): void {
    this.initForm();
  }
  initForm() {
    this.formCreate = this.formBuilder.group({
      name: ["", [Validators.required, Validators.minLength(2)]],
      description: [""],
    });
  }
  onSubmit() {
    if (this.formCreate.invalid) {
      return;
    } else {
      this.blockUI.start();
      this.symptomService.symptomCreate(this.formCreate.value).subscribe(
        (response) => {
          this.blockUI.stop();
          if (response.code == "SUCCESS") {
            this.formCreate.reset();
            this.snackBar.open("Tạo mới thành công", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          } else {
            this.blockUI.stop();
            this.snackBar.open("Tạo mới thất bại", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          }
        },
        (err) => {
          this.snackBar.open("Lỗi hệ thống", "Ẩn", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "bottom",
          });
        }
      );
    }
  }
  onCancel() {
    this.formCreate.reset();
  }
}
