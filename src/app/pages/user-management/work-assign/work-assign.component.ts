import { ListOfUserResponse } from "./../../../core/models/user";
import { ActivatedRoute } from "@angular/router";
import { Router } from "@angular/router";
import { MatSnackBar } from "@angular/material/snack-bar";
import { ListOfUser, User, UserResponse } from "app/core/models/user";
import { Component, OnInit } from "@angular/core";
import { TokenStorageService } from "app/_service/JWT_service/token-storage.service";
import { AccountService } from "app/_service/manager_service/account.service";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import { AddressService } from "app/_service/manager_service/address.service";
import { ListOfProvince } from "app/core/models/address/province";
import { ListOfDistrict } from "app/core/models/address/district";

@Component({
  selector: "app-work-assign",
  templateUrl: "./work-assign.component.html",
  styleUrls: ["./work-assign.component.css"],
})
export class WorkAssignComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  formAssign: FormGroup;

  listOfMedicalStaff: ListOfUser;
  listOfDoctor: ListOfUser;
  listOfUserResponse: ListOfUserResponse;
  listOfPatient: ListOfUser;
  listOfNotAssignedPatient: ListOfUser;
  currentUser: User;
  doctorId: number;
  patientsId = "";
  listOfUser: ListOfUser;
  listOfProvince: ListOfProvince;
  listOfDistrict: ListOfDistrict;
  defaultProvinceId: number;
  defaultDistrictId: number;
  defaultVillageId: number;
  isSuperAdmin = false;
  isAdmin = false;
  isMod = false;
  p: number = 1;

  defaultDoctorId: number;

  constructor(
    private accountService: AccountService,
    private tokenStorageService: TokenStorageService,
    private snackBar: MatSnackBar,
    private addressService: AddressService,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.route.queryParams.subscribe((params) => {
      this.defaultVillageId = params.id;
    });
  }

  ngOnInit(): void {
    this.currentUser = this.tokenStorageService.getUser();
    this.initForm();
    if (this.currentUser.roleName === "Super admin") {
      this.isSuperAdmin = true;
      this.getProvince();
    } else if (this.currentUser.roleName === "Trung tâm Y tế") {
      this.isAdmin = true;
      this.getMedicalStaff(this.currentUser.districtId);
      // this.getUser(this.defaultVillageId);
    } else {
      this.isMod = true;
      this.getUser(this.currentUser.villageId);
      this.formAssign.patchValue({ villageId: this.currentUser.villageId });
    }
  }

  initForm(villageId?) {
    this.formAssign = this.formBuilder.group({
      villageId: villageId,
      patientIds: this.formBuilder.array([], [Validators.required]),
      doctorId: [this.defaultDoctorId, [Validators.required]],
    });
  }

  //superAdmin
  getProvince() {
    this.blockUI.start();
    this.addressService.getProvince().subscribe(
      (data) => {
        this.blockUI.stop();
        this.listOfProvince = data.data;
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }
  onChangeProvinceId(value) {
    this.defaultProvinceId = value;
    this.listOfDistrict = null;
    this.listOfMedicalStaff = null;
    this.listOfDoctor = null;
    this.listOfPatient = null;
    this.getDistrict(value);
  }
  getDistrict(provinceId) {
    this.blockUI.start();
    this.addressService.getDistrict(provinceId).subscribe(
      (data) => {
        this.blockUI.stop();
        this.listOfDistrict = data.data;
      },
      (err) => {
        this.blockUI.stop();
        this.defaultDistrictId = null;
        this.defaultVillageId = null;
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }
  onChangeDistrictId(value) {
    this.defaultDistrictId = value;
    this.defaultVillageId = null;
    this.listOfMedicalStaff = null;
    this.listOfDoctor = null;
    this.listOfPatient = null;
    this.getMedicalStaff(value);
    this.formAssign.patchValue({
      villageId: null,
      // patientIds : [null],
    });
  }
  getMedicalStaff(districtId) {
    this.blockUI.start();
    this.accountService.getMedicalStaff(districtId).subscribe(
      (data) => {
        this.blockUI.stop();
        if (data.data.length != 0) {
          this.listOfMedicalStaff = data.data;
          //on default value
          // this.defaultVillageId = this.listOfMedicalStaff[0]?.village.villageId;
          // this.getUser(this.defaultVillageId);
        } else {
          this.blockUI.stop();
          this.snackBar.open("Chưa có trạm y tế nào", "Ẩn", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "bottom",
          });
        }
      }
      // (err) => {
      //   this.blockUI.stop();
      //   this.snackBar.open("Lỗi hệ thống", "Ẩn", {
      //     duration: 3000,
      //     horizontalPosition: "right",
      //     verticalPosition: "bottom",
      //   });
      // }
    );
  }
  onChangeVillage(value) {
    this.defaultVillageId = value;
    this.listOfDoctor = null;
    this.listOfPatient = null;
    this.getUser(this.defaultVillageId);
    this.formAssign.patchValue({
      villageId: value,
    });
  }

  onCheckboxChange(event) {
    const patientIds: FormArray = this.formAssign.get(
      "patientIds"
    ) as FormArray;

    if (event.target.checked) {
      patientIds.push(new FormControl(event.target.value));
    } else {
      let i: number = 0;
      patientIds.controls.forEach((item: FormControl) => {
        if (item.value == event.target.value) {
          patientIds.removeAt(i);
          return;
        }
        i++;
      });
    }
  }

  getMedicalStaffByAdmin(districtId) {
    this.blockUI.start();
    this.accountService.getMedicalStaff(districtId).subscribe(
      (data) => {
        this.blockUI.stop();
        this.listOfMedicalStaff = data.data;
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }

  getDoctorByVillageId(villageId) {
    this.blockUI.start();
    this.accountService.getDoctorByVillageId2(villageId).subscribe(
      (data) => {
        this.blockUI.stop();
        if (data.data.length != 0) {
          this.listOfUserResponse = data;
          this.listOfDoctor = this.listOfUserResponse.data;
        } else {
          this.snackBar.open("Chưa có bác sĩ", "Ẩn", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "bottom",
          });
        }
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }
  getPatientByVillageId(villageId) {
    this.blockUI.start();
    this.accountService.getPatientByVillageId(villageId).subscribe(
      (data) => {
        this.blockUI.stop();
        if (data.data.length != 0) {
          this.listOfNotAssignedPatient = data.data;
          this.getNotAssignPatient(data.data);
        } else {
          this.snackBar.open("Chưa có bệnh nhân", "Ẩn", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "bottom",
          });
        }
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }
  getUser(villageId) {
    this.getDoctorByVillageId(villageId);
    this.getPatientByVillageId(villageId);
  }
  onAssign() {
    // console.log(this.formAssign.value);
    // for (var index in this.formAssign.value) {
    //   this.patientsId += this.formAssign.value[index];
    // }
    console.log(this.formAssign.value);
    if (this.formAssign.invalid) {
      this.snackBar.open("Chưa chọn bác sĩ/bệnh nhân", "Ẩn", {
        duration: 3000,
        horizontalPosition: "right",
        verticalPosition: "bottom",
      });
      return;
    } else {
      this.blockUI.start();
      this.accountService.updateWorkAssign(this.formAssign.value).subscribe(
        (res) => {
          if (res) {
            this.blockUI.stop();
            if(this.isSuperAdmin){
              this.getPatientByVillageId(this.defaultVillageId);
            }
            else if (this.isAdmin) {
              this.getPatientByVillageId(this.defaultVillageId);
            } else {
              this.getPatientByVillageId(this.currentUser.villageId);
            }
            this.snackBar.open("Thành công", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          }
        },
        (err) => {
          this.blockUI.stop();
          this.snackBar.open("Lỗi hệ thống", "Ẩn", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "bottom",
          });
        }
      );
    }
  }
  getNotAssignPatient(data) {
    const notAssign = 0;
    this.listOfPatient = data.filter((item) => item.isAssign === notAssign);
  }

  getFullName(lastName, surName, firstName) {
    return lastName + " " + surName + " " + firstName;
  }
  getAddress(village, district, province) {
    return village + " - " + district + " - " + province;
  }
}
