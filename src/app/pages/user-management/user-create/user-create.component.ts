import { ListOfVillage } from "./../../../core/models/address/village";
import { ListOfProvince } from "./../../../core/models/address/province";
import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatSnackBar } from "@angular/material/snack-bar";
import { AccountService } from "app/_service/manager_service/account.service";
import { AddressService } from "app/_service/manager_service/address.service";
import { FileManagementService } from "app/_service/manager_service/file-management.service";
import { ListOfUser, User } from "app/core/models/user";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { ListOfDistrict } from "app/core/models/address/district";
import { TokenStorageService } from "app/_service/JWT_service/token-storage.service";
import { MatTabChangeEvent } from "@angular/material/tabs";
import { formatDate } from "@angular/common";
import * as moment from "moment";

@Component({
  selector: "app-user-create",
  templateUrl: "./user-create.component.html",
  styleUrls: ["./user-create.component.css"],
})
export class UserCreateComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  @ViewChild("tabGroup") tabGroup;
  @ViewChild("fileUpload") fileUpload;

  formManager: FormGroup;
  formMedicalStaff: FormGroup;
  formDoctor: FormGroup;
  formPatient: FormGroup;
  formFinal: FormGroup;

  selectedFiles?: FileList;
  selectedFileName = "";
  currentFile?: File;
  message = "";
  errorMsg = "";
  currentUser: User;

  userId: number;

  listOfProvince: ListOfProvince;
  listOfDistrict: ListOfDistrict;
  listOfVillage: ListOfVillage;

  defaultProvince: number;
  defaultDistrict: number;
  defaultVillage: number;
  listOfMedicalStaff: ListOfUser;
  fullName: string;
  isAdmin = false;
  isSuperAdmin = false;
  isMod = false;
  currentTab: number;
  villageId: number;
  districtId: number;
  firstName: string;
  surName: string;
  lastName: string;
  currentMedicalStaffId: number;
  maxDate = moment().format("YYYY-MM-DD");

  constructor(
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private accountService: AccountService,
    private addressService: AddressService,
    private FileService: FileManagementService,
    private tokenStorageService: TokenStorageService
  ) {
    this.formManager = this.formBuilder.group({
      fullName: ["", [Validators.required]],
      username: ["", [Validators.required, Validators.minLength(3)]],
      password: null,
      firstname: [""],
      surname: [""],
      lastname: [""],
      role: ["", [Validators.required]],
      email: [
        "",
        [
          Validators.required,
          Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$"),
        ],
      ],
      gender: null,
      dateOfBirth: null,
      medicalStaff: null,
      address: [""],
      phone: ["", Validators.pattern("[0-9]{10}")],
      hospital: null,
      village: [, [Validators.required]],
    });
    this.formMedicalStaff = this.formBuilder.group({
      fullName: ["", [Validators.required]],
      username: ["", [Validators.required, Validators.minLength(3)]],
      password: null,
      firstname: [""],
      surname: [""],
      lastname: [""],
      role: ["", [Validators.required]],
      email: [
        "",
        [
          Validators.required,
          Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$"),
        ],
      ],
      gender: null,
      dateOfBirth: null,
      medicalStaff: null,
      address: [""],
      phone: ["", Validators.pattern("[0-9]{10}")],
      hospital: null,
      village: [, [Validators.required]],
    });
    this.formDoctor = this.formBuilder.group({
      fullName: [
        "",
        [
          Validators.required,
          Validators.pattern(
            "^([a-z A-Z ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêếẾìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹs]+)$"
          ),
        ],
      ],
      username: ["", [Validators.required, Validators.minLength(3)]],
      password: null,
      firstname: [""],
      surname: [""],
      lastname: [""],
      role: ["", [Validators.required]],
      email: [
        "",
        [Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$")],
      ],
      gender: [],
      dateOfBirth: [],
      medicalStaff: [],
      address: [""],
      phone: ["", [Validators.required, Validators.pattern("[0-9]{10}")]],
      hospital: [""],
      village: ["", [Validators.required]],
    });
    this.formPatient = this.formBuilder.group({
      fullName: [
        "",
        [
          Validators.required,
          Validators.pattern(
            "^([a-z A-Z ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêếẾìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹs]+)$"
          ),
        ],
      ],
      username: ["", [Validators.required, Validators.minLength(3)]],
      password: null,
      firstname: [""],
      surname: [""],
      lastname: [""],
      role: ["", [Validators.required]],
      email: [
        "",
        [Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$")],
      ],
      gender: [],
      dateOfBirth: [],
      medicalStaff: null,
      address: [""],
      phone: ["", [Validators.required, Validators.pattern("[0-9]{10}")]],
      hospital: null,
      village: ["", [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.currentUser = this.tokenStorageService.getUser();
    if (this.currentUser.roleName === "Super admin") {
      this.isSuperAdmin = true;
      this.getProvince();
    } else if (this.currentUser.roleName === "Trung tâm Y tế") {
      this.isAdmin = true;
      this.getVillage(this.currentUser.districtId);
      this.getMedicalStaff(this.currentUser.districtId);
    } else {
      this.isMod = true;
      this.getMedicalStaff(this.currentUser.districtId);
    }
  }

  convertFullName(name) {
    var names = name.split(" ");
    this.lastName = names[0].toString();
    this.firstName = names[names.length - 1].toString();
    this.surName = "";
    for (var i = 1; i != names.length; i++) {
      if (i != names.length - 1) {
        if (i == 1) {
          this.surName = this.surName + names[i];
        } else {
          this.surName = this.surName + " " + names[i];
        }
      }
    }
    if (this.currentTab == 1) {
      this.formManager.patchValue({
        firstname: this.firstName,
        surname: this.surName,
        lastname: this.lastName,
      });
    } else if (this.currentTab == 2) {
      this.formMedicalStaff.patchValue({
        firstname: this.firstName,
        surname: this.surName,
        lastname: this.lastName,
      });
    } else if (this.currentTab == 3) {
      this.formDoctor.patchValue({
        firstname: this.firstName,
        surname: this.surName,
        lastname: this.lastName,
      });
    } else {
      this.formPatient.patchValue({
        firstname: this.firstName,
        surname: this.surName,
        lastname: this.lastName,
      });
    }
  }
  //CreateForm
  ngAfterViewInit() {
    if (this.isSuperAdmin) {
      this.currentTab = this.tabGroup.selectedIndex + 1;
    } else if (this.isAdmin) {
      this.currentTab = this.tabGroup.selectedIndex + 2;
    } else {
      this.currentTab = this.tabGroup.selectedIndex + 3;
    }

    this.formManager.patchValue({
      role: this.currentTab,
    });
    this.formMedicalStaff.patchValue({
      role: this.currentTab,
    });
    this.formPatient.patchValue({
      role: this.currentTab,
    });
    this.formDoctor.patchValue({
      role: this.currentTab,
    });
  }

  public tabChanged(tabChangeEvent: MatTabChangeEvent): void {
    if (this.isSuperAdmin) {
      this.currentTab = tabChangeEvent.index + 1;
    } else if (this.isAdmin) {
      this.currentTab = tabChangeEvent.index + 2;
    } else {
      this.currentTab = tabChangeEvent.index + 3;
    }

    this.formManager.patchValue({
      village: this.villageId,
      role: this.currentTab,
    });
    this.formMedicalStaff.patchValue({
      village: this.villageId,
      role: this.currentTab,
    });
    this.formPatient.patchValue({
      village: this.villageId,
      role: this.currentTab,
    });
    this.formDoctor.patchValue({
      village: this.villageId,
      role: this.currentTab,
    });
    // console.log(tabChangeEvent);
    // console.log('this.currentTab2' + this.currentTab);
  }

  //patchUserName from phone
  onPatchUserNameFromPhone(event) {
    if (this.currentTab === 3 || this.currentTab === 4) {
      this.formDoctor.patchValue({
        username: event.target.value,
      });
      this.formPatient.patchValue({
        username: event.target.value,
      });
    }
  }
  //patchUserName from email
  onPatchUserNameFromEmail(event) {
    if (this.currentTab === 1) {
      this.formManager.patchValue({
        username: event.target.value,
      });
    } else if (this.currentTab === 2) {
      this.formMedicalStaff.patchValue({
        username: event.target.value,
      });
    }
  }

  onPatchvillageId(villageId) {
    if (this.currentTab == 1) {
      this.formManager.patchValue({
        village: villageId,
      });
    } else if (this.currentTab == 2) {
      this.formMedicalStaff.patchValue({
        village: villageId,
      });
    } else if (this.currentTab == 3) {
      this.formDoctor.patchValue({
        village: villageId,
      });
    } else {
      this.formPatient.patchValue({
        village: villageId,
      });
    }
  }
  getMedicalStaffByAdmin(districtId) {
    this.blockUI.start();
    this.accountService.getMedicalStaff(districtId).subscribe((data) => {
      this.blockUI.stop();
      if (data.data.length != 0) {
        this.listOfMedicalStaff = data.data;
      } else {
        this.blockUI.stop();
        this.snackBar.open("Chưa có trạm y tế nào", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    });
  }

  //VillageId from currentUser
  getVillage(districtId) {
    this.blockUI.start();
    this.addressService.getVillage(districtId).subscribe(
      (data) => {
        this.blockUI.stop();
        this.listOfVillage = data.data;
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }
  //getMedicalStaff
  getMedicalStaff(districtId) {
    this.blockUI.start();
    this.accountService.getMedicalStaff2(districtId).subscribe(
      (data) => {
        this.blockUI.stop();
        this.listOfMedicalStaff = data;
        if (this.isMod) {
          for (var i = 0; i <= this.listOfMedicalStaff.length; i++) {
            if (this.currentUser.userId == this.listOfMedicalStaff[i]?.userId) {
              this.currentMedicalStaffId = this.listOfMedicalStaff[
                i
              ].medicalStaffId;
            }
          }
          this.formDoctor.patchValue({
            medicalStaff: this.listOfMedicalStaff[0].medicalStaffId,
          });
        }
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }

  onSubmit() {
    if (this.currentTab === 1) {
      this.formFinal = this.formManager;
    } else if (this.currentTab === 2) {
      this.formFinal = this.formMedicalStaff;
    } else if (this.currentTab === 3) {
      this.formFinal = this.formDoctor;
    } else {
      this.formFinal = this.formPatient;
    }
    console.log(this.currentTab);

    if (this.isMod) {
      if (this.currentTab === 3) {
        this.formDoctor.patchValue({
          medicalStaff: this.currentMedicalStaffId,
        });
      }
      this.formFinal.patchValue({
        village: this.currentUser.villageId,
      });
    }
    console.log(this.formFinal);
    if (this.formFinal.invalid) {
      this.snackBar.open("Chưa đủ hoặc sai thông tin", "Ẩn", {
        duration: 3000,
        horizontalPosition: "right",
        verticalPosition: "bottom",
      });
      return;
    } else {
      this.blockUI.start();
      this.accountService.accountSignIn2(this.formFinal.value).subscribe(
        (response) => {
          this.blockUI.stop();
          if (response) {
            this.formManager.reset();
            this.formMedicalStaff.reset();
            this.formDoctor.reset();
            this.formPatient.reset();
            this.formManager.patchValue({
              role: this.currentTab,
            });
            this.formMedicalStaff.patchValue({
              role: this.currentTab,
            });
            this.formDoctor.patchValue({
              role: this.currentTab,
            });
            this.formPatient.patchValue({
              role: this.currentTab,
            });
            this.snackBar.open("Tạo mới thành công", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          } else if (response.message === "MANAGER is exist!") {
            this.snackBar.open("Tài khoản đã tồn tại", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          } else if (response.message === "Phone number is existed in data") {
            this.snackBar.open("Số điện thoại đã tồn tại", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          }
        },
        (err) => {
          this.blockUI.stop();
          // console.log(err.error.error);
          if (err.error.error === "PHONENUMBER_EXISTED") {
            this.snackBar.open("Số điện thoại đã tồn tại", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          } else if (err.error.error === "EMAIL_EXIST") {
            this.snackBar.open("Email đã tồn tại", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          } else if (err.error.error === "MANAGER_EXIST") {
            this.snackBar.open("Trung tâm y tế đã tồn tại", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          } else if (err.error.error === "MEDICAL_STAFF_EXIST") {
            this.snackBar.open("Trạm y tế đã tồn tại", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          } else {
            this.snackBar.open("Tạo mới thất bại", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          }
        }
      );
    }
  }
  onCancel() {
    console.log("cancel: " + this.currentTab);
    if (this.currentTab == 1) {
      this.formManager.reset();
      this.formManager.patchValue({
        role: this.currentTab,
      });
    } else if (this.currentTab == 2) {
      this.formMedicalStaff.reset();
      this.formMedicalStaff.patchValue({
        role: this.currentTab,
      });
    } else if (this.currentTab == 3) {
      this.formDoctor.reset();
      this.formDoctor.patchValue({
        role: this.currentTab,
      });
    } else {
      this.formPatient.reset();
      this.formPatient.patchValue({
        role: this.currentTab,
      });
    }
  }
  //sample file download
  onSampleFileDownload() {
    const link = document.createElement("a");
    link.setAttribute("target", "_blank");
    link.setAttribute(
      "href",
      "https://healthcaresystemstorage.s3.us-east-2.amazonaws.com/ExampleExcel/HealthCare-File_mau_import_nguoi_dung_2.xlsx"
    );
    link.setAttribute("download", `products.csv`);
    document.body.appendChild(link);
    link.click();
    link.remove();
  }
  //file Upload
  onSelectFile(event: any): void {
    this.blockUI.start();
    this.selectedFiles = event.target.files;
    this.selectedFileName = event.target.files[0].name;
    if (this.selectedFiles) {
      const file: File | null = this.selectedFiles.item(0);
      if (file) {
        this.currentFile = file;
        this.FileService.importFile(this.currentFile).subscribe((response) => {
          this.blockUI.stop();
          if (response.code == "SUCCESS") {
            this.snackBar.open("Tải lên thành công", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          } else {
            this.blockUI.stop();
            this.snackBar.open("Tải lên thất bại", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          }
        });
        this.fileUpload.nativeElement.value = "";
      }
    }
  }

  //superAdmin
  getProvince() {
    this.blockUI.start();
    this.addressService.getProvince().subscribe(
      (data) => {
        this.blockUI.stop();
        this.listOfProvince = data.data;
        this.defaultProvince = this.listOfProvince[0].provinceId;
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }
  onChangeProvinceId(value) {
    this.villageId = null;
    this.districtId = null;
    if (this.currentTab == 1) {
      this.formManager.patchValue({});
    }

    this.getDistrict(value);
  }
  getDistrict(provinceId) {
    this.blockUI.start();
    this.addressService.getDistrict(provinceId).subscribe(
      (data) => {
        this.blockUI.stop();
        this.listOfDistrict = data.data;
        this.defaultDistrict = this.listOfDistrict[0].districtId;
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }
  onChangeDistrictId(value) {
    this.districtId = value;
    this.villageId = null;
    this.formMedicalStaff.patchValue({ village: this.villageId });
    this.formDoctor.patchValue({ village: this.villageId });
    this.formPatient.patchValue({ village: this.villageId });
    // this.getMedicalStaff(value);
    this.getVillage(value);
    this.getMedicalStaff(value);
  }
  onChangeMedicalStaff(event) {
    const staff = this.listOfMedicalStaff.filter(
      (item) => item.medicalStaffId == event.target.value
    );
    if (staff && staff.length !== 0) {
      this.formDoctor.patchValue({
        medicalStaff: staff[0].medicalStaffId,
        village: staff[0].villageId,
      });
    }
  }

  // onChangeVillage(villageValue) {
  //   this.villageId = villageValue;

  //   this.formMedicalStaff.patchValue({ village: this.villageId });
  //   this.formDoctor.patchValue({ village: this.villageId });
  //   this.formPatient.patchValue({ village: this.villageId });
  // }

  getFullName(lastName, surName, firstName) {
    return lastName + " " + surName + " " + firstName;
  }
  onChangeDate(date) {
    // var dateFormated = formatDate(date, "yyyy-MM-dd", "en-US");
    // console.log(formatDate(date, "yyyy-MM-dd", "en-US"));
    this.formDoctor.patchValue({
      dateOfBirth: formatDate(date, "yyyy-MM-dd", "en-US"),
    });
    this.formPatient.patchValue({
      dateOfBirth: formatDate(date, "yyyy-MM-dd", "en-US"),
    });
  }
}
