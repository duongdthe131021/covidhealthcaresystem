import { RequestService } from "app/_service/manager_service/request.service";
import { BlockUI } from "ng-block-ui";
import { Component, Inject, OnInit } from "@angular/core";
import { NgBlockUI } from "ng-block-ui";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { MatSnackBar } from "@angular/material/snack-bar";
import { DeleteComponent } from "../delete/delete.component";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ListOfRequest } from "app/core/models/request";

@Component({
  selector: "app-request-response",
  templateUrl: "./request-response.component.html",
  styleUrls: ["./request-response.component.css"],
})
export class RequestResponseComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  formRequest: FormGroup;
  listOfRequest: ListOfRequest;

  message = "PHẢN HỒI YÊU CẦU NGƯỜI DÙNG";
  confirmButtonText = "Gửi";

  constructor(
    @Inject(MAT_DIALOG_DATA) private data: any,
    private dialogRef: MatDialogRef<DeleteComponent>,
    private snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private requestService: RequestService
  ) {}

  ngOnInit(): void {
    this.initForm();
  }
  initForm() {
    this.formRequest = this.formBuilder.group({
      note: [""],
      status: ["", Validators.required],
    });
  }
  onConfirmClick(): void {
    if (this.data) {
      this.formRequest.patchValue({ status: this.data.status });
      this.onResponse(this.data.requestId);
      // this.dialogRef.afterClosed().subscribe((data) => {
      //   this.getRequestByVillageId(2, this.data.villageId);
      // });
    }
  }
  onResponse(requestId) {
    if (this.formRequest.invalid) {
      return;
    } else {
      this.blockUI.start();
      this.requestService
        .updateRequest(this.formRequest.value, requestId)
        .subscribe(
          (response) => {
            this.blockUI.stop();
            if (response) {
              this.dialogRef.close();
              this.snackBar.open("Xử lý hành công", "Ẩn", {
                duration: 3000,
                horizontalPosition: "right",
                verticalPosition: "bottom",
              });
            }
          },
          (err) => {
            this.blockUI.stop();
            this.dialogRef.close();
            this.snackBar.open("Xử lý thất bại", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          }
        );
    }
  }
  getRequestByVillageId(requestStatus, villageId) {
    this.blockUI.start();
    this.listOfRequest = [];
    this.requestService.getRequestByStatus(requestStatus, villageId).subscribe(
      (data) => {
        if (data) {
          this.listOfRequest = data;
          this.blockUI.stop();
        } else {
          this.blockUI.stop();
          this.snackBar.open("Tải trang lỗi", "Ẩn", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "bottom",
          });
        }
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }
}
