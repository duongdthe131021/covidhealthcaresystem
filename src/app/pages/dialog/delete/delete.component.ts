import { DeleteService } from "../../../_service/manager_service/delete.service";
import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { MatSnackBar } from "@angular/material/snack-bar";
import { TestResult } from "app/core/models/test-result";
import { TestResultService } from "app/_service/manager_service/test-result.service";

@Component({
  selector: "app-delete",
  templateUrl: "./delete.component.html",
  styleUrls: ["./delete.component.css"],
})
export class DeleteComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;

  testResults: TestResult[];
  testResultsFilter: TestResult[];

  message: string = "Xóa dữ liệu?";
  confirmButtonText = "Xóa";
  cancelButtonText = "Hủy";
  apiData: any;
  constructor(
    @Inject(MAT_DIALOG_DATA) private data: any,
    private dialogRef: MatDialogRef<DeleteComponent>,
    private deleteService: DeleteService,
    private testResultService: TestResultService,
    private snackBar: MatSnackBar
  ) {
    dialogRef.disableClose = true;
    if (data) {
      this.message = data.message || this.message;
      if (data.buttonText) {
        this.confirmButtonText = data.buttonText.ok || this.confirmButtonText;
        this.cancelButtonText = data.buttonText.cancel || this.cancelButtonText;
      }
    }
  }
  ngOnInit(): void {}
  onConfirmClick(): void {
    if (this.data.type === "exercise-detail") {
      this.blockUI.start();
      this.apiData = `exercise_detail/delete?exerciseDetailId=${this.data.id}`;
      this.deleteService.deleteById2(this.apiData).subscribe(
        (response) => {
          if (response === "true") {
            this.blockUI.stop();
            this.dialogRef.close();
            this.snackBar.open("Xóa thành công", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          } else {
            this.blockUI.stop();
            this.snackBar.open("Xóa thất bại", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          }
        },
        (err) => {
          this.blockUI.stop();
          this.snackBar.open("Lỗi hệ thống", "Ẩn", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "bottom",
          });
        }
      );
    } else {
      if (this.data.type === "medicine") {
        this.apiData = `medicine/delete?medicineId=${this.data.id}`;
      } else if (this.data.type === "symptom") {
        this.apiData = `symptom/delete/${this.data.id}`;
      } else if (this.data.type === "exercise") {
        this.apiData = `exercise/delete?id=${this.data.id}`;
      } else if (this.data.type === "user") {
        this.apiData = `user/delete/${this.data.id}`;
      } else if (this.data.type === "testResult") {
        this.apiData = `result/delete/${this.data.id}`;
      }
      // console.log(this.apiData);
      this.blockUI.start();
      this.deleteService.deleteById(this.apiData).subscribe(
        (response) => {
          if (response === "true") {
            this.blockUI.stop();
            if (this.data.type === "testResult") {
              this.dialogRef.close();
              // this.getTestResult(this.data.villageId);
            } else {
              this.dialogRef.close();
            }
            this.snackBar.open("Xóa thành công", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          } else {
            this.blockUI.stop();
            if (this.data.type === "testResult") {
              this.dialogRef.close();
            }
            this.dialogRef.close();
            this.snackBar.open("Xóa thất bại", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          }
        },
        (err) => {
          this.blockUI.stop();
          this.snackBar.open("Lỗi hệ thống", "Ẩn", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "bottom",
          });
        }
      );
    }
  }
  getTestResult(villageId) {
    this.blockUI.start();
    var numberTest: number[] = [];
    this.testResultService.getTestResult(villageId).subscribe(
      (data) => {
        this.blockUI.stop();
        this.testResults = data.data;
        //getTestResultByTestNumber
        this.testResultsFilter = this.testResults.filter(
          (item) => item.numberTest == this.data.numberTest
        );
        // this.dialogRef.close(this.testResultsFilter);
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }
}
