import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";


import { SidebarModule } from './sidebar/sidebar.module';
import { FooterModule } from './shared/footer/footer.module';
import { NavbarModule} from './shared/navbar/navbar.module';
import { FixedPluginModule} from './shared/fixedplugin/fixedplugin.module';

import { AppComponent } from './app.component';
import { AppRoutes } from './app.routing';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { ReportComponent } from './pages/report/report.component';
import { ExerciseComponent } from './pages/exercise/exercise.component';
import { MedicinesComponent } from './pages/medicines/medicines.component';
import { RequestComponent } from './pages/request/request.component';
import { LoginComponent } from './pages/login/login.component';
import { MatDialogModule } from "@angular/material/dialog";
import { ForgotPasswordComponent } from './pages/login/forgot-password/forgot-password.component';
import { SymptomComponent } from './pages/symptom/symptom.component';
import { PaginationComponent } from './shared/pagination/pagination.component';
import { ReportDetailComponent } from './pages/report/report-detail/report-detail.component';
import { MedicalStaffComponent } from "./pages/user-management/medical-staff/medical-staff.component";
import { UserCreateComponent } from './pages/user-management/user-create/user-create.component';
import { ExerciseCreateComponent } from './pages/exercise/exercise-create/exercise-create.component';
import { MedicinesCreateComponent } from './pages/medicines/medicines-create/medicines-create.component';
import { SymptomCreateComponent } from './pages/symptom/symptom-create/symptom-create.component';
import { MedicineDetailComponent } from './pages/medicines/medicine-detail/medicine-detail.component';
import { NoAccessLayoutComponent } from './layouts/no-access-layout/no-access-layout.component';
import { SymptomDetailComponent } from './pages/symptom/symptom-detail/symptom-detail.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { DeleteComponent } from './pages/dialog/delete/delete.component';
import { MatSelectModule } from '@angular/material/select';
import { UserProfileComponent } from './pages/user-management/user-profile/user-profile.component';
import { SearchComponent } from './pages/search/search.component';
import { BlockUIModule } from 'ng-block-ui';
import { MatFormFieldModule } from "@angular/material/form-field";
import { ExerciseDetailComponent } from './pages/exercise/exercise-detail/exercise-detail.component';
import { NgxPaginationModule } from "ngx-pagination";
import { MatTabsModule } from "@angular/material/tabs";
import { PermissionBlockComponent } from './pages/errors/permission-block/permission-block.component';
import { ExerciseEditComponent } from './pages/exercise/exercise-edit/exercise-edit.component';
import { MedicineEditComponent } from './pages/medicines/medicine-edit/medicine-edit.component';
import { PasswordChangeComponent } from './pages/currentUser/password-change/password-change.component';
import { TestResultComponent } from './pages/test-result/test-result.component';
import { TestResultCreateComponent } from './pages/test-result/test-result-create/test-result-create.component';
import { TestResultDetailComponent } from './pages/test-result/test-result-detail/test-result-detail.component';
import { ExerciseStepEditComponent } from './pages/exercise/exercise-step-edit/exercise-step-edit.component';
import { NewPasswordComponent } from './pages/login/new-password/new-password.component';
import { MatNativeDateModule } from '@angular/material/core';
import { MatInputModule } from '@angular/material/input';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { ConfirmDialogComponent } from './pages/dialog/confirm-dialog/confirm-dialog.component';
import { ReportByUserComponent } from './pages/report/report-by-user/report-by-user.component';
import { WorkAssignComponent } from "./pages/user-management/work-assign/work-assign.component";
import { AssignEditComponent } from './pages/user-management/assign-edit/assign-edit.component';
import { ExerciseMenuComponent } from './pages/dialog/exercise-menu/exercise-menu.component';
import { RequestResponseComponent } from './pages/dialog/request-response/request-response.component';
import { UserActiveComponent } from './pages/user-management/user-active/user-active.component';
import { RequestCreateComponent } from './pages/request/request-create/request-create.component';


@NgModule({
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    MedicalStaffComponent,
    ReportComponent,
    ExerciseComponent,
    MedicinesComponent,
    RequestComponent,
    LoginComponent,
    ForgotPasswordComponent,
    SymptomComponent,
    PaginationComponent,
    ReportDetailComponent,
    UserCreateComponent,
    ExerciseCreateComponent,
    MedicinesCreateComponent,
    SymptomCreateComponent,
    MedicineDetailComponent,
    NoAccessLayoutComponent,
    SymptomDetailComponent,
    DeleteComponent,
    UserProfileComponent,
    SearchComponent,
    ExerciseDetailComponent,
    PermissionBlockComponent,
    ExerciseEditComponent,
    MedicineEditComponent,
    PasswordChangeComponent,
    TestResultComponent,
    TestResultCreateComponent,
    TestResultDetailComponent,
    WorkAssignComponent,
    ExerciseStepEditComponent,
    NewPasswordComponent,
    ConfirmDialogComponent,
    ReportByUserComponent,
    AssignEditComponent,
    ExerciseMenuComponent,
    RequestResponseComponent,
    UserActiveComponent,
    RequestCreateComponent,

  ],
  imports: [
    BrowserAnimationsModule,
    RouterModule.forRoot(AppRoutes,{
      useHash: true
    }),
    SidebarModule,
    NavbarModule,
    HttpClientModule,
    ReactiveFormsModule,
    BlockUIModule.forRoot(),
    FooterModule,
    MatSnackBarModule,
    MatSelectModule,
    FixedPluginModule,
    MatDialogModule,
    MatTabsModule,
    NgxPaginationModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    // MatMenuModule,
    
  ],
  exports: [PaginationComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
